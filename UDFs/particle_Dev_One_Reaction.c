/******************************************************************************
 * One Reaction Devolatilisation model
 ******************************************************************************
 * Function:    provides the one reaction devolatilisation model.
 *              
 * Input:       
 * Tracked_Particle *p		pointer to particle
 * dh_dev							heat released by devolatilisation
 * dm_spec_dev					mass of species released by devolatilisation
 * Particle_Composition pc
 *
 * Output:      
 * dh_dev				heat released by devolatilisation (=0)
 * dm_spec_dev		vector of species masses released by devolatilisation
 *****************************************************************************/
#include <math.h>
#include <stdio.h>
#include "Preamble.h"

void One_Reaction(Tracked_Particle *p, double *dh_dev, double *dm_spec_dev,
                  Particle_Composition *pc) {

	double Rgas = UNIVERSAL_GAS_CONSTANT;
	double dev_a1 = DEV_A1; 						/* preexponential factor */
	double dev_e1 = DEV_E1; 						/* activation energy */
	double t_p = P_T(p); 								/* particle temperature */
	double dt = P_DT(p); 								/* duration of particle time step */
	double m_p = P_MASS(p); 						/* particle mass */
	double m_p_init = P_INIT_MASS(p); 	/* particle initialisation mass */
	/* End input */
	
	double dm_dev; 	/* mass release rate of volatiles */
	double kin_rate; 	/* kinetic rate of volatiles reaction */
	
	kin_rate = dev_a1*exp(-dev_e1/(Rgas*t_p));
	
	dm_dev = (-1)*pc->vol_remain*(1.-exp(-kin_rate*dt))/dt;

	/* if 99% of volatiles have evaporated, release remaining */
	if (pc->vol_remain < 0.01*COAL_VOL*m_p_init){
		dm_dev = (-1) * pc->vol_remain / dt;
	}
	
	/* Begin output */
	dm_spec_dev[Y_SPECIE_VOL] = dm_dev;
}