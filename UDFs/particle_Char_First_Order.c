/******************************************************************************
 * First Order Model
 ******************************************************************************
 * Function:    provides the first order char burnout model.
 *              
 * Input:       
 * Tracked_Particle  *p    pointer to particle structure p
 * dh_char							heat released by char burnout
 * dm_spec_char				mass of species released by char burnout
 *
 * Output:      
 *	dh_char							heat released by char burnout
 * dm_spec_char				mass of species released by char burnout 
 *
 *****************************************************************************/
#include <math.h>
#include <stdio.h>
#include "Preamble.h"

void First_Order(Tracked_Particle *p, double *dh_char, double *dm_spec_char,
                 Particle_Composition *pc) {
	double const Rgas = UNIVERSAL_GAS_CONSTANT;
	double const pi = PI; /* ratio between the circumference of a circle to its diameter in Euclidean geometry */
				 
	double Hchar_O2, Hchar_CO2, Hchar_H2O; /* heat from heterogenous reaction J/kg */
	
	/* model coefficients */
	double char_c1_O2 = CHAR_C1_O2; 			/* diffusion rate coefficient O2 */
	double char_c2_O2 = CHAR_C2_O2; 			/* preexponential factor kinetic rate O2 */
	double char_e2_O2 = CHAR_E2_O2; 			/* activation energy kinetic rate O2 */
	double char_c1_CO2 = CHAR_C1_CO2; 		/* diffusion rate coefficient CO2 */ 
	double char_c21_CO2 = CHAR_C21_CO2; 	/* preexponential factor kinetic rate CO2, low temperatures */
	double char_c22_CO2 = CHAR_C22_CO2; 	/* activation energy kinetic rate CO2, low temperatures */ 
	double char_e21_CO2 = CHAR_E21_CO2; 	/* preexponential factor kinetic rate CO2, high temperatures */ 
	double char_e22_CO2 = CHAR_E22_CO2; 	/* activation energy kinetic rate CO2, high temperatures */ 
	double char_c1_H2O = CHAR_C1_H2O; 		/* diffusion rate coefficient H2O */ 
	double char_c21_H2O = CHAR_C21_H2O; 	/* preexponential factor kinetic rate H2O, low temperatures */ 
	double char_c22_H2O = CHAR_C22_H2O; 	/* activation energy kinetic rate H2O, low temperatures */ 
	double char_e21_H2O = CHAR_E21_H2O; 	/* preexponential factor kinetic rate H2O, high temperatures */ 
	double char_e22_H2O = CHAR_E22_H2O; 	/* activation energy kinetic rate H2O, high temperatures */
		
	double t_p = P_T(p); 								/* particle temperature */
	double m_p = P_MASS(p); 						/* particle mass */
	double m_p_init = P_INIT_MASS(p); 	/* particle mass at initialisation */
	double d_p =  P_DIAM(p); 						/* particle diameter */
	double dt = P_DT(p); 								/* duration of particle time step */
	double f_ash = COAL_ASH;    					/* fraction of ash in particle*/
	
	double phi2 = Phi2(PHI2, t_p); 			/* char combustion to CO or CO2 */
	/* fraction of heat from heterogeneous reaction going to particle */
	double chfac = MATERIAL_PROP(P_MATERIAL(p),PROP_hreact_frac);
	
	double t_g = C_T(P_CELL(p),P_CELL_THREAD(p)); 		/* gas temperature */
	double rho_g = C_R(P_CELL(p),P_CELL_THREAD(p)); 	/* gas density */
	double volfrac_o2 = C_YI(P_CELL(p),P_CELL_THREAD(p),Y_SPECIE_O2); 		/* volume fraction of O2 */
	double volfrac_co2 = C_YI(P_CELL(p),P_CELL_THREAD(p),Y_SPECIE_CO2); 	/* volume fraction of CO2 */
	double volfrac_h2o = C_YI(P_CELL(p),P_CELL_THREAD(p),Y_SPECIE_H2O); 	/* volume fraction of H2O */

	/* End input */
	
	double dm_O2 = 0; 									/* mass release rates for O2 */
	double dm_CO2 = 0; 								/* mass release rates for CO2 */
	double dm_H2O = 0; 								/* mass release rates for H2O */
	double rate_diff_O2, rate_kin_O2; 		/* reaction rates for O2 */
	double rate_diff_CO2, rate_kin_CO2; 	/* reaction rates for CO2 */
	double rate_diff_H2O, rate_kin_H2O; 	/* reaction rates for H2O */
	double m_p_new; 									/* particle mass after this time step */
	double mass_rem_fraction; 					/* mass released / mass release predicted */
	double m_char_sum; 								/* total mass released in this time step */

	/* Common part of diffusion coefficients for oxidizers */
	double D0_ = pow(0.5*(t_p+t_g),0.75)/d_p; 
 
/* Hchar calculates the heat of heterogeneous reaction as a function of product species (CO and/or CO2		*/
	Hchar_O2 = (-HOF_CO/M_C)*(2.-2./phi2)+(-HOF_CO2/M_C)*(2./phi2-1);	/* =   9.20833e6 J/kgCarbon */	
	Hchar_CO2 = HOF_CO2/M_C - 2. * HOF_CO/M_C;												/* = -14.37683e6 J/kgCarbon */
	Hchar_H2O = HOF_H2O/M_C - HOF_CO/M_C;														/* = -10.94483e6 J/kgCarbon */
		
	

	/* mass released due to O2 reaction */
	/* why is this phi2 here? Because it belongs here!*/
	rate_diff_O2 = phi2*char_c1_O2*D0_;  /* diffusion rate, the copy given by FLUENT is wrong!!! */
        rate_kin_O2 = char_c2_O2*exp(-char_e2_O2/(Rgas*t_p));		/* kinetic rate   */

	/* Gleichung 23.3-69 FLUENT Handbuch 6.2.16 */
	dm_O2 = - pi*d_p*d_p * rho_g * Rgas * volfrac_o2 * t_g * \
	          rate_diff_O2*rate_kin_O2/((rate_diff_O2+rate_kin_O2)*M_O2);


	if(NUMBER_CHAR_REACTIONS == 3) {
		/* mass released due to CO2 reaction */
		rate_diff_CO2 = char_c1_CO2*D0_;/* diffusion rate */

		/* kinetic rate */ 
		if (t_p<=1223.) {
			/* low temperature */
			rate_kin_CO2 = char_c21_CO2*exp(-char_e21_CO2/(Rgas*t_p));
		} else {
			/* high temperature */
			rate_kin_CO2 = char_c22_CO2*exp(-char_e22_CO2/(Rgas*t_p));
		}

		/* kg/s of char mass lost by the particle - first order apparent kinetics */
		dm_CO2 = - pi*d_p*d_p * rho_g * Rgas * volfrac_co2 * t_g * \
	          rate_diff_CO2*rate_kin_CO2/((rate_diff_CO2+rate_kin_CO2)*M_CO2);

		/* mass released due to H2O reaction */	
		rate_diff_H2O = char_c1_H2O*D0_;/* diffusion rate */

		/* kinetic rate */ 
		if (t_p<=1233.) {
			/* low temperature */
			rate_kin_H2O = char_c21_H2O*exp(-char_e21_H2O/(Rgas*t_p));
		} else {
			/* high temperature */
			rate_kin_H2O = char_c22_H2O*exp(-char_e22_H2O/(Rgas*t_p));
		}	

		/* kg/s of char mass lost by the particle - first order model */
		dm_H2O = - pi*d_p*d_p * rho_g * Rgas * volfrac_h2o * t_g * \
	          rate_diff_H2O*rate_kin_H2O/((rate_diff_H2O+rate_kin_H2O)*M_H2O);

	} /* end of 3 reactions */
 		
/* ----- end of calculation of the mass released from particle due to heterogeneous reaction ------*/ 
	m_char_sum = (dm_O2 + dm_CO2 + dm_H2O)*dt; /* total char mass released */

/* Check if the mass released from the last time step is overpredicted. 
   If yes, a linear interpolation is made.*/
	if ((-1)*m_char_sum > pc->char_remain) {
		m_p_new = m_p_init*f_ash;	
		
		mass_rem_fraction = (-1)*pc->char_remain/m_char_sum;

		dm_O2  = dm_O2  * mass_rem_fraction;
		dm_CO2 = dm_CO2 * mass_rem_fraction;
		dm_H2O = dm_H2O * mass_rem_fraction; 
	}

/* Mass source due to reaction of char with oxygen */	
	dm_spec_char[Y_SPECIE_CO] += (2.-2./phi2)*M_CO/M_C * dm_O2;
	dm_spec_char[Y_SPECIE_O2] -= 1./phi2*M_O2 /M_C * dm_O2;
	dm_spec_char[Y_SPECIE_CO2] += (2./phi2-1.)*M_CO2/M_C * dm_O2;
	 
/* Not available in Fluent standard solver */	
	if(NUMBER_CHAR_REACTIONS == 3) {
/* Mass source due to reaction of char with CO2 */	
		dm_spec_char[Y_SPECIE_CO] += 2.*M_CO /M_C * dm_CO2;
		dm_spec_char[Y_SPECIE_CO2] -= M_CO2/M_C * dm_CO2;	

/* Mass source due to reaction of char with H2O */
		dm_spec_char[Y_SPECIE_CO] += M_CO /M_C * dm_H2O;
		dm_spec_char[Y_SPECIE_H2O] -= M_H2O/M_C * dm_H2O;
	}

/* Summation of heat released by char burnout from oxidation and gasification reactions */
	*dh_char = -chfac*(Hchar_O2*dm_O2 + Hchar_CO2*dm_CO2 + Hchar_H2O*dm_H2O);
}
