/*
 *	This is unpublished proprietary source code of WSA, RWTH-AACHEN..
 */

/* This file is a template for all the user input, that has to be put into the
   UDF-source prior to compilation.
   Usage:
   - Remove the trailing ".template" from the file name 
   - Select the appropriate models and setting for your case
   - Compile, load and run */

/*****************************************************************************
 * Environment to run the particle UDF in
 ****************************************************************************/
#define UDF_ENVIRONMENT FLUENT 
/* FLUENT or PLUGFLOW */

/*****************************************************************************
 * Solid fuel properties, as received basis 
 ****************************************************************************/
/* Ultimate analysis */

/* Rhenish coal */
#define COAL_C 0.6905		/* carbon   in coal */
#define COAL_H 0.0483		/* hydrogen in coal */
#define COAL_N 0.0069  	/* nitrogen in coal */
#define COAL_S 0.0030  	/* sulphur  in coal */
/* oxygen, water and ash is the remaining part up to one */
#define COAL_O 0.2513 		/* oxygen   in coal */

/* fraction of elements, which reside in volatiles, rest is considered to 
   be in char.
   So 0.5 means 50% of this element is in volatiles but 
   not 50% of volatiles are this element! */

#define FRAC_H_VOL 1.
#define FRAC_O_VOL 1.
#define FRAC_N_VOL 0.8 /* COAL_VOL/(COAL_CHAR+COAL_VOL)  for proportional distribution of nitrogen to mass between char and volatiles */
#define FRAC_S_VOL 1.
#define FRAC_C_VOL (COAL_VOL-(FRAC_H_VOL*COAL_H + FRAC_O_VOL*COAL_O + FRAC_N_VOL*COAL_N + FRAC_S_VOL*COAL_S))/COAL_C

/* Definition of elements and corresponding molar masses */
#define NUMBER_OF_ELEMENTS 6	/* 5 plus water */
#define EL_C 0
#define EL_H 1
#define EL_O 2
#define EL_N 3
#define EL_S 4
#define EL_H2O 5

#define MM_EL {M_C, M_H, M_O, M_N, M_S, M_H2O}

/* Proximate analysis */
#define COAL_H2O 0.1215		/* water in coal */
#define COAL_VOL 0.4242		/* volatiles in coal */
#define COAL_CHAR 0.3999	/* char in coal */
#define COAL_ASH (1-COAL_H2O-COAL_VOL-COAL_CHAR)	/* ash in coal */
/****************************************************************************/

/******************************************************************************
 * Model definitions
 *****************************************************************************/

/* defines which particle type to use */
#define PARTICLE_TYPE COMBUSTING_PARTICLE
/* COMBUSTING_PARTICLE (or: *not fully implemented* DROPLET) */


/* defines the activity of particle models: */
#define PARTICLE_MODEL_ORDER PARALLEL_MODELS 
/* SERIAL_MODELS or PARALLEL_MODELS */


/* defines the operating conditions: */
#define OPERATING_CONDITIONS OXYCOAL_CONDITIONS
/* AIR_CONDITIONS or OXYCOAL_CONDITIONS */


/* defines the devolatilization model to use: */
#define DEVOLATILIZATION_MODEL ONE_REACTION
/* ONE_REACTION, TWO_REACTIONS, (experimental: CPD or CPD_NLG) */

  #define DEV_A1 29058 				/* Preexponential factor */
  #define DEV_E1 4.2879e+07 		/* Activation energy */
  #define DEV_ALPHA1 0.3611 	/* same as amount of volatiles */
  #define DEV_A2 1.46e+13 			/* Preexponential factor */
  #define DEV_E2 2.51e+08 			/* Activation energy */
  #define DEV_ALPHA2 0.7222 	/* same as twice the amaount of volatiles */


/* defines the char model to use: */
#define CHAR_MODEL FIRST_ORDER

/* TG: Not Used */
/* defines the coal type */
#define COAL_TYPE LIGNITE
/* BITUMINOUS or LIGNITE */

/* FIRST_ORDER	Field, 1969 
   CBK 		Hurt, 1998 (half order w/o ash inhibition and thermal annealing)
   (experimental:
   INTRINSIC 	to be tested !!
   LANGMUIR	to be tested !!)*/

  #if OPERATING_CONDITIONS == OXYCOAL_CONDITIONS
    #define CHAR_C1_O2 7.43e-13
    #define CHAR_C1_CO2 1e-10
    #define CHAR_C1_H2O 2.84e-12
  #else
    #define CHAR_C1_O2 2.5e-12
    #define CHAR_C1_CO2 2.21e-12
    #define CHAR_C1_H2O 3.56e-12
  #endif
  #define CHAR_C2_O2 188.6
  #define CHAR_E2_O2 1.286e+8
  #define CHAR_C21_CO2 0.135e-3
  #define CHAR_C22_CO2 6.35e-3
  #define CHAR_E21_CO2 135518200.
  #define CHAR_E22_CO2 162123000.
  #define CHAR_C21_H2O 0.319
  #define CHAR_C22_H2O 1.92e-3
  #define CHAR_E21_H2O 208016280.
  #define CHAR_E22_H2O 146991520.

  #define ACTE_O2 83740000.0 	/* activation energy for half order model, */
  #define ACTE_CO2 115000000.0 	/* activation energy for half order model, J/kmol */
  #define ACTE_H2O 112500000.0 	/* activation energy for half order model, */

/* Defines the ratio between CO and CO2 released during char burnout */
  #define PHI2 PHI2_CO
/* Possible options are PHI2_CO, PHI2_CO2, PHI2_ARTHUR and PHI2_HURT */
#define thermalAnealing 0 


/* defines the No of char reactions:	1 - oxygen,
 * 					3 - O2, CO2, H2O*/
#define NUMBER_CHAR_REACTIONS 3

/* defines wether or not radiation is used */
#define DPM_RADIATION_P 1
#define EPSILON_P 0.9

/* CPD-parameter calculator: */
/* 		1 - calculates the chemical structure of coal(13C NMR) based on
 *		    the coal's elemental composition and ASTM volatile matter content
 *		    (Genetti, Fletcher and Pugmire, Energy & Fuels, 13:60, 1999) 
 * 		0 - values defined in coalprop.h are used 					*/
#define CPD_PARAMETER_CALCULATOR 0
#define CPD_LIGHT_GAS_SUBMODEL 0

/* Definition of the species in the gas mixture */
/* this order should correspond to the order in FLUENT Materials panel */
#define NUMBER_OF_SPECIES 7
#define Y_SPECIE_VOL 0
#define Y_SPECIE_O2 1
#define Y_SPECIE_H2O 2
#define Y_SPECIE_CO 3
#define Y_SPECIE_SO2 4
#define Y_SPECIE_N2 5
#define Y_SPECIE_CO2 6

/* not in use
#define Y_SPECIE_CH4 8
#define Y_SPECIE_TAR 9
*/

/* elemental composition of species
   rows represent species, columns represent elements */
   /*C,H,O,N,S,?*/
#define N_EL_SPEC {{1.42, 1.99, 0.65, 0.0205, 0.0038, 0}, \
					{0,0,2,0,0,0}, \
					{0,2,1,0,0,0}, \
					{1,0,1,0,0,0}, \
					{0,0,2,0,1,0}, \
					{0,0,0,2,0,0}, \
					{1,0,2,0,0,0}}

/* settings for light gases in devolatilisation */
/* this value determines the fraction of the respective molecule of the volatiles being released */
/*#define K_CH4_VOL 0.0452*/
#define K_CO2_VOL 0.0
#define K_CO_VOL 0.0
#define K_H2_VOL 0.0


/* vt ########################################## */
/*settings for using NO-Modell: 0-off 1-on */
#define NO_MODEL 0

/* NO-Model Variety: 
0 - only Coal-N conversion to Specie HCN, NH3, NO or N2 without NO-creating and -reburning reactions
1 -  Foertsch, 
2 - Mitchell, 
3 - De Soete, origin Model + NO-Reburning on char-surface taken from Lel,
4 - De Soete, modefed Model by Magel/Schnell
5 - Chen 
6 - Chen modifiziert nach Taniguchi fuer OH als Oxidator 
7 - Stadler */
#define NO_MODEL_VAR 7

/* NO-char reaction model 
1 - Chan 1983 nach Yang 1998
2 - Chen 2001 (lignite)
3 - Chen 2001 (bituminous)
4 - Commandre 2002
5 - Fiveland 1991
6 - Foertsch 1998
7 - Jones 1999
8 - Levy 1981
9 - Mitchell and Tarbell 1982
10 - Schoenenbeck 2004
11 - Sun 2009 
12 - Chen 1996 nach Vitali Tregubow */
#define NO_CHAR_MODEL_VAR 13

/* vol-N  */
#define K_HCN_VOL 1.0
#define K_NH3_VOL 0.
#define K_N0_VOL 0.
#define K_N2_VOL 0.

/* char-N */
#define K_HCN_CHAR 0.10
#define K_NH3_CHAR 0.10
#define K_NO_CHAR 0.07
#define K_N2_CHAR 0.73

/* 0- approximation: Char-N-release proportial to carbon release from Char dm_n/dm_c = m_n(t)/m_c(t)*/
/* 1- use expression  dm_n/dm_c = m_n(t)/m_c(t)*(1+0.4*Uc), where Uc=m_c(t)/m_c(0)   /FOERTSCH, BAXTER/ */
#define N_RELEASE_FROM_CHAR 1
/* end vt #########################################*/