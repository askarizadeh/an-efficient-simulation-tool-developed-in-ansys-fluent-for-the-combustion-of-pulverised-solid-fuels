/******************************************************************************
 * Carbon Burnout Kinetic
 ******************************************************************************
 * Function:    provides the CBK char burnout model.
 *              
 * Input:       
 * Tracked_Particle  *p    pointer to particle structure p
 * dh_char							heat released by char burnout
 * dm_spec_char				mass of species released by char burnout
 *
 * Output:      
 * dh_char							heat released by char burnout
 * dm_spec_char				mass of species released by char burnout 
 *
 * Caveats:	thermal annealing and ash inhibition are to be implemented 
 *****************************************************************************/
#include <math.h>
#include <stdio.h>
#include "Preamble.h"

void Carbon_Burnout_Kinetic(Tracked_Particle *p, double *dh_char, 
                            double *dm_spec_char, Particle_Composition *pc) { 

	double const Rgas = UNIVERSAL_GAS_CONSTANT;
	double const pi = PI; 								/* ratio between the circumference of a circle to its diameter in Euclidean geometry */

	double t_p = P_T(p); 								/* particle temperature */
	double m_p_init = P_INIT_MASS(p); 	/* particle mass at initialisation */
	double d_p =  P_DIAM(p); 						/* particle diameter */
	double dt = P_DT(p); 								/* duration of particle time step */
		
	/* fraction of heat from heterogeneous reaction going to particle */
	double chfac = MATERIAL_PROP(P_MATERIAL(p),PROP_hreact_frac);
	/* chfac = 0.7*PHI2 - 0.4;*/					/* chfac splits the energy between solid and gas phases */
	double phi2 = Phi2(PHI2, t_p); 			/* char combustion to CO or CO2 */
	
	double t_g = C_T(P_CELL(p),P_CELL_THREAD(p)); 												/* gas temperature */
	double rho_g = C_R(P_CELL(p),P_CELL_THREAD(p)); 											/* gas density */
	double mfrac_o2 = C_YI(P_CELL(p),P_CELL_THREAD(p),Y_SPECIE_O2); 			/* mass fraction of O2 */
	double mfrac_co2 = C_YI(P_CELL(p),P_CELL_THREAD(p),Y_SPECIE_CO2); 		/* mass fraction of CO2 */
	double mfrac_h2o = C_YI(P_CELL(p),P_CELL_THREAD(p),Y_SPECIE_H2O); 	/* mass fraction of H2O */

 	double t_film, t_film_34;
	double t_p_mean; 				/* Mean temperature between previous and actual time step */

	double Hchar_O2, Hchar_CO2, Hchar_H2O;	/* heat from heterogenous reaction J/kg	*/

	double dm_O2=0.;
	double dm_CO2=0.;
	double dm_H2O=0.;
	double mass_rem_fraction, m_char_sum;

	double coal_c_daf = COAL_C/(1. - COAL_ASH - COAL_H2O);

	double pressure_O2;    			/* partial pressure of o2 (Pa) */
	double pressure_CO2;				/* partial pressure of co2 (Pa) */
	double pressure_H2O;			/* partial pressure of h2o (Pa) */
 

 
	double rct, rct_CO2, rct_H2O, sqroot;
	
	double ac_o2, ac_co2, ac_h2o;
	double char_c1_O2  = CHAR_C1_O2;
	double char_c1_H2O = CHAR_C1_H2O; 
 	double char_c1_CO2 = CHAR_C1_CO2;

	double acte_o2 = ACTE_O2;
	double acte_co2 = ACTE_CO2;
 	double acte_h2o = ACTE_H2O;
  
	double rate_diff_O2, rate_diff_CO2, rate_diff_H2O;
	double rate_kin_O2, rate_kin_CO2, rate_kin_H2O;

/* Hchar calculates the heat of heterogeneous reaction as a function of product species (CO and/or CO2) */	
	Hchar_O2 = (-HOF_CO/M_C)*(2.-2./phi2)+(-HOF_CO2/M_C)*(2./phi2-1);	/* =   9.20833e6 J/kgCarbon */	
	Hchar_CO2 = HOF_CO2/M_C - 2. * HOF_CO/M_C;												/* = -14.37683e6 J/kgCarbon */
	Hchar_H2O = HOF_H2O/M_C - HOF_CO/M_C;														/* = -10.94483e6 J/kgCarbon */
		
/* End of input parameters */

	
	/* Ac - Pre-exp of apparent kinetics, kg/m^2-s-Pa^0.5*/	
	ac_o2  = 10.*exp(8.12 - 0.0715*100.*coal_c_daf)/318.0;  /* Hurt, 1998 */
	ac_co2 = 0.0732; /* Mann, Combustion and Flame 99: 147-156, 1994 */
	ac_h2o = 0.0782; /* Mann, Combustion and Flame 99: 147-156, 1994 */
	
	t_p_mean = t_p; /* A better approximation would be the mean temperature of particle in previous and current time step */
	t_film = 0.5*(t_p_mean+t_g);
	t_film_34 = pow(t_film,0.75);

 	rate_diff_O2 = phi2*char_c1_O2*t_film_34/d_p; /* diffusion rate, the copy given by FLUENT is wrong!!! */
	rate_kin_O2 = ac_o2 * exp(-acte_o2/Rgas/t_p); /* kinetic rate */
	
	pressure_O2 = rho_g*Rgas*t_g*mfrac_o2/M_O2;
	sqroot = rate_kin_O2 * rate_kin_O2 + 4.0 * pressure_O2 * rate_diff_O2 * rate_diff_O2;
	sqroot = pow(sqroot,0.5);
	rct = rate_kin_O2 * (sqroot - rate_kin_O2)/(2.0 * rate_diff_O2); /* total rate=kin/diff	*/
	dm_O2 = -pi*d_p*d_p*rct; /* kg/s of char mass lost by the particle - half order model */ 
	
	if(NUMBER_CHAR_REACTIONS == 3) {
	 	rate_diff_CO2 = char_c1_CO2*t_film_34/d_p; /* diffusion rate */
		rate_kin_CO2	= ac_co2 * exp(-acte_co2/Rgas/t_p);	/* kinetic rate */
		
		pressure_CO2 = rho_g*Rgas*t_g*mfrac_co2/M_CO2; 
		sqroot = rate_kin_CO2 * rate_kin_CO2 + 4.0 * pressure_CO2 * rate_diff_CO2 * rate_diff_CO2;
		sqroot = pow(sqroot,0.5); 
		rct_CO2	= rate_kin_CO2 * (sqroot - rate_kin_CO2)/(2.0 * rate_diff_CO2); /* total rate=kin/diff */
		dm_CO2 = -pi*d_p*d_p*rct_CO2; /* kg/s of char mass lossed by the particle - half  order model */    
		
 		rate_diff_H2O = char_c1_H2O*t_film_34/d_p; 				/* diffusion rate */
		rate_kin_H2O = ac_h2o * exp(-acte_h2o/Rgas/t_p);		/* kinetic rate */	
		
		pressure_H2O = rho_g*Rgas*t_g*mfrac_h2o/M_H2O; 
		sqroot = rate_kin_H2O * rate_kin_H2O + 4.0 * pressure_H2O * rate_diff_H2O * rate_diff_H2O;
		sqroot = pow(sqroot,0.5); 
		rct_H2O	= rate_kin_H2O * (sqroot - rate_kin_H2O)/(2.0 * rate_diff_H2O);	/* total rate=kin/diff */
		dm_H2O = -pi*d_p*d_p*rct_H2O;	/* kg/s of char mass lossed by the particle - half  order model */
		
	} /* end of 3 reactions */
	
/* end of calculation of the mass released from particle due to heterogeneous reaction */ 

	m_char_sum = (dm_O2 + dm_CO2 + dm_H2O)*dt; /* total char mass released */

/* Check if the mass released from the last time step is overpredicted. 
   If yes, a linear interpolation is made.*/
	if ((-1)*m_char_sum > pc->char_remain) {
  
		mass_rem_fraction = (-1)*pc->char_remain/m_char_sum;

		dm_O2  = dm_O2  * mass_rem_fraction;
		dm_CO2 = dm_CO2 * mass_rem_fraction;
		dm_H2O = dm_H2O * mass_rem_fraction;
	}
	
/* Begin output */
/* Mass source due to reaction of char with oxygen */	
	dm_spec_char[Y_SPECIE_CO] += (2.-2./phi2)*M_CO/M_C * dm_O2;
	dm_spec_char[Y_SPECIE_O2] -= 1./phi2*M_O2 /M_C * dm_O2;
	dm_spec_char[Y_SPECIE_CO2] += (2./phi2-1.)*M_CO2/M_C * dm_O2;
	 
	if(NUMBER_CHAR_REACTIONS == 3) {
/* Mass source due to reaction of char with CO2 */	
		dm_spec_char[Y_SPECIE_CO] += 2.*M_CO /M_C * dm_CO2;
		dm_spec_char[Y_SPECIE_CO2] -= M_CO2/M_C * dm_CO2;	

/* Mass source due to reaction of char with H2O */
		/*dm_spec_char[Y_SPECIE_H2] += M_H2 /M_C * dm_H2O;*/
		dm_spec_char[Y_SPECIE_CO] += M_CO /M_C * dm_H2O;
		dm_spec_char[Y_SPECIE_H2O] -= M_H2O/M_C * dm_H2O;
	}

	*dh_char = -chfac*(Hchar_O2*dm_O2 + Hchar_CO2*dm_CO2 + Hchar_H2O*dm_H2O);
}
