/*****************************************************************************
	The wavelength dependency of the gas radiative properties under
	oxyfuel conditions is approximated by a modified weighted-sum-of-grey-
	gases (WSGG) model proposed by Bordbar et al. [1].
	
	[1] Bordbar MH, Wecel G, Hyppänen T. A line by line based weighted sum
	of gray gases model for inhomogeneous CO2–H2O mixture in oxy-fired
	combustion. Combust Flame 2014;161(9):2435–45. 
	http://dx.doi.org/10.1016/j.combustflame.2014.03.013.
*****************************************************************************/
#include <stdlib.h>
#include "udf.h"
#include "materials.h"
DEFINE_WSGGM_ABS_COEFF(UD_WSGGM, c, t, xi, p_t, s, soot_conc, Tcell, nb, ab_wsggm, ab_soot)
{
	Material *m = THREAD_MATERIAL(t);
	int ico2 = mixture_specie_index(m, "co2");
	int ih2o = mixture_specie_index(m, "h2o");
	int Ng = 4 ;
	int i, j, k ;
	
	double b ;
	double epsilon = 0 ;
	double Tref = 1200 ;
	double Tr = Tcell / Tref ;
	double CO2_molf = xi[ico2] ;
	double H2O_molf = xi[ih2o] ;
	double Mr = H2O_molf / (CO2_molf+1e-15) ;	
	Mr = fmin(Mr,4) ;
	double a[5] = {0, 0, 0, 0, 0} ;
	double K[5] = {0, 0, 0, 0, 0} ;

	double Coeff[4][5][5] = { { {0.7412956, -0.5244441, 0.5822860, -0.2096994, 0.0242031}, 
													{-0.9412652, 0.2799577, -0.7672319, 0.3204027, -0.0391017}, 
													{0.8531866, 0.0823075, 0.5289430, -0.2468463, 0.0310940}, 
													{-0.3342806, 0.1474987, -0.4160689, 0.1697627, -0.0204066}, 
													{0.0431436, -0.0688622, 0.1109773, -0.0420861, 0.0049188} },
												 { {0.1552073, -0.4862117, 0.3668088, -0.1055508, 0.0105857}, 
													{0.6755648, 1.4092710, -1.3834490, 0.4575210, -0.0501976}, 
													{-1.1253940, -0.5913199, 0.9085441, -0.3334201, 0.0384236}, 
													{0.6040543, -0.0553385, -0.1733014, 0.0791608, -0.0098934}, 
													{-0.1105453, 0.0464663, -0.0016129, -0.0035398, 0.0006121} },
												{ {0.2550242, 0.3805403, -0.4249709, 0.1429446, -0.0157408}, 
												   {-0.6065428, 0.3494024, 0.1853509, -0.1013694, 0.0130244}, 
												   {0.8123855, -1.1020090, 0.4046178, -0.0811822, 0.0062981}, 
												   {-0.4532290, 0.6784475, -0.3432603, 0.0883088, -0.0084152}, 
												   {0.0869309, -0.1306996, 0.0741446, -0.0202929, 0.0020110} },
												{ {-0.0345199, 0.2656726, -0.1225365, 0.0300151, -0.0028205}, 
												   {0.4112046, -0.5728350, 0.2924490, -0.0798076, 0.0079966}, 
												   {-0.5055995, 0.4579559, -0.2616436, 0.0764841, -0.0079084}, 
												   {0.2317509, -0.1656759, 0.1052608, -0.0321935, 0.0033870}, 
												   {-0.0375491, 0.0229520, -0.0160047, 0.0050463, -0.0005364} } 
											    } ;

    double d[4][5] = { 
										{0.0340429, 0.0652305, -0.0463685, 0.0138684, -0.0014450}, 
										{0.3509457, 0.7465138, -0.5293090, 0.1594423, -0.0166326}, 
										{4.5707400, 2.1680670, -1.4989010, 0.4917165, -0.0542999}, 
										{109.81690, -50.923590, 23.432360, -5.1638920, 0.4393889} 
									};
	
	for (i = 0; i < Ng; i++)
	{
        for (j = 0; j < Ng + 1; j++)
		{
			b = 0.0 ;
			for (k = 0; k < Ng + 1; k++)
			{
				b = b + Coeff[i][j][k] * pow(Mr , k) ;
			}
			a[i+1] = a[i+1] + b * pow(Tr , j) ;
		}
    }
	a[0] = 1 - a[1] - a[2] - a[3] - a[4] ;
		
	for (i = 0; i < Ng; i++)
	{
		for (k = 0; k < Ng + 1; k++)
		{
			K[i + 1] = K[i + 1] + d[i][k] * pow(Mr , k) ;
		}
	}
	
	
	if (s < 0.0001) {
        for (i = 0; i < Ng + 1; i++)
		{
			epsilon = epsilon + a[i] * K[i] * p_t * (H2O_molf + CO2_molf) * s ;
		}
	} else {
        for (i = 0; i < Ng + 1; i++)
		{
			epsilon = epsilon + a[i] * (1 - exp(-K[i] * p_t * (H2O_molf + CO2_molf) * s)) ;
		}
		epsilon = -(1/s) * log(1 - epsilon) ;
    }
	/*
	FILE *str ;
	str = fopen ("Output.txt", "a") ;
	fprintf (str, "Mr = %4.8f\n", Mr) ;
	fclose (str) ;
	*/
	*ab_wsggm = epsilon ;
}