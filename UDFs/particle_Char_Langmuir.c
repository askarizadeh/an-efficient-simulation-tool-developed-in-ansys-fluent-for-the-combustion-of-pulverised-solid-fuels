/******************************************************************************
 * Langmuir
 ******************************************************************************
 * Function:    provides the Langmuir char burnout model.
 *              
 * Input:       
 * Tracked_Particle  *p    pointer to particle structure p
 * dh_char							heat released by char burnout
 * dm_spec_char				mass of species released by char burnout
 *
 * Output:      
 * dh_char				heat released by char burnout
 * dm_spec_char	mass of species released by char burnout 
 *****************************************************************************/
#include "Preamble.h"
#include <math.h>
#include <stdio.h>

double Ratio(double T) {
	/* The two functions are to calculate the stochiometric factor of heterogeneous reaction
	   based on determining the CO/CO2 molar ratio in the primary heterogeneous products 
	   by the empirical correlation 
	   CO/CO2=Ac exp(-E/R/Tp)  
	   (Hurt and Mitchell, 24th Symposium on Combustion, pp1243-1250, 1992)
	  
	  Tp       particle temperature, K
	  Aratio   Pre-exponential factor, Ac=3E8
	  Eratio   Activation energy, Ec=60kcal/mole=2.5122E5 kJ/kmole */

	double RuGas = UNIVERSAL_GAS_CONSTANT/1000;
	double Aratio = 3.E8;
	double Eratio = 2.5122E5;	/* kJ/kmole */

	/* CO/CO2 molar ratio */     
	double Ratio = Aratio * exp(-Eratio/RuGas/T);
	return Ratio;
}

double Stochio(double T){ 
	/* stochiometric factor */
	double Stochio = (2.0+2.0*Ratio(T))/(2.0+Ratio(T));
	return Stochio;
}



void Langmuir(Tracked_Particle *p, double *dh_char, double *dm_spec_char,
              Particle_Composition *pc) {


	/*double ka;*/
	double kad, kde, kd;
	/* kinetic parameters for Langmuir Isotherm of char reaction */
    /* double aKad0 = 0.7;  			ka0 -- Pre-exp of adsorption reaction */
	/* double aKde0 = 26170.0;  	kd0 -- Pre-exp of desorption reaction */
	/* double Ead = 42035.0;   		Ead -- Activation Energy of adsorption */
	/* double Ede = 137955.0; 	Ede -- Activation Energy of desorption */
	
	/* Essenhigh and Mescher, 26th Symposium on Combustion, 1996 */
	double aKad0 = 4.35;			/* ka0 -- Pre-exp of adsorption reaction, kg/m^2/s */
	double aKde0 = 45.0;			/* kd0 -- Pre-exp of desorption reaction, kg/m^2/s */
	double Ead = 31395.0;		/* Ead -- Activation Energy of adsorption, J/mole */
	double Ede = 100464.0;		/* Ede -- Activation Energy of desorption, J/mole */
	
	float Hchar_O2, Hchar_CO2, Hchar_H2O;	/* heat from heterogenous reaction J/kg */
	float chfac;
	double phi2 = Phi2(PHI2, P_T(p));
	
	double Rugas = UNIVERSAL_GAS_CONSTANT/1000;		/* J/mol/K */
	/*double Ru = 0.08206;*/
	
	double Rgas = UNIVERSAL_GAS_CONSTANT; 	/* Rgas=8314 J/kmol */
	double dt = P_DT(p);		/* time step in Lagrangian tracking */
	
	double eps;
	double q;
	double pressure_O2,pressure_CO2,pressure_H2O;    		/*partial pressures (Pa)*/
	double pml_O2, pml_CO2, pml_H2O;
	pml_CO2 = 0.;
	pml_H2O = 0.;		
	pml_O2 = 0.;
	
	chfac = MATERIAL_PROP(P_MATERIAL(p),PROP_hreact_frac);
/* Hchar calculates the heat of heterogeneous reaction as a function of product species (CO and/or CO2)		*/
	Hchar_O2 = (-HOF_CO/M_C)*(2.-2./phi2)+(-HOF_CO2/M_C)*(2./phi2-1);	/* =   9.20833e6 J/kgCarbon */	
	Hchar_CO2 = HOF_CO2/M_C - 2. * HOF_CO/M_C;				/* = -14.37683e6 J/kgCarbon */
	Hchar_H2O = HOF_H2O/M_C - HOF_CO/M_C;						/* = -10.94483e6 J/kgCarbon */
	
	/* adsorption rate constant */
	/* ka = (P/P0)*pow((Tp/T0),0.5)*3./8.*pow((rho0*P0/2./PI),0.5) */
	kad = aKad0*exp(-Ead/Rugas/P_T(p));		/* kg/m2/s */
	
	/* film temperature */
	double Tm = (P_T(p)+C_T(P_CELL(p), P_CELL_THREAD(p)))/2.0;
		
	/* calculating oxygen diffusion coefficient in nitrogen (air) */
	/*double Db = 1.523E-09*pow(Tm,1.67);*/	/* m^2/s */
	
	/* desorption rate constant */
	kde = aKde0*exp(-Ede/Rugas/P_T(p)); 	/* kg/m2/s */

	
	/* Effective diffusivity */
      	/* kd = 24.0*Stochio(Tp)*Db/Dp/Ru/Tm;*/	/* kg/m2/s/atm */
	kd = PHYSCON * phi2 * pow(Tm,0.75) / P_DIAM(p);	/* kg/m^2/s/Pa */
	
	pressure_O2 = C_R(P_CELL(p),P_CELL_THREAD(p))*Rgas* \
		C_T(P_CELL(p),P_CELL_THREAD(p))* \
		C_YI(P_CELL(p),P_CELL_THREAD(p),Y_SPECIE_O2)/M_O2;	/* O2 partial pressure, Pa */
	eps = 3.6;
	/* apparent reaction rate, q kg/m^2/s */
	q = 1./(1.0/(eps*kad*C_YI(P_CELL(p),P_CELL_THREAD(p),Y_SPECIE_O2))+1.0/(eps*kde)+1.0/(kd*pressure_O2));
	
	double dmdt_O2, dmdt_CO2, dmdt_H2O;
	dmdt_O2 = PI*pow(P_DIAM(p),2.0)*q;		/* dMp/dt */
	/*printf("in Langmuir dmp: %e kd: %e %e %e\n",dmdt_O2,1.0/(eps*kad*volFrac_o2), \
		1.0/(eps*kde),1.0/(kd*pressure_O2));*/
	pml_O2 = -dmdt_O2*dt;	

	
/******************** Needs to be checked ********************/
/**************************************************************/	
	if(NUMBER_CHAR_REACTIONS == 3) {

		pressure_CO2 = C_R(P_CELL(p),P_CELL_THREAD(p))*Rgas* \
			C_T(P_CELL(p),P_CELL_THREAD(p))* \
			C_YI(P_CELL(p),P_CELL_THREAD(p),Y_SPECIE_CO2)/M_CO2;	/* CO2 partial pressure at total pressure = 3 atm */
		q = 1./(1.0/(eps*kad*pressure_CO2)+1.0/(eps*kde)+1.0/(kd*pressure_CO2));
		dmdt_CO2 = PI*pow(P_DIAM(p),2.0)*q;		/* dMp/dt */
		pml_CO2 = dmdt_CO2*dt;

		
		pressure_H2O = C_R(P_CELL(p),P_CELL_THREAD(p))*Rgas* \
			C_T(P_CELL(p),P_CELL_THREAD(p))* \
			C_YI(P_CELL(p),P_CELL_THREAD(p),Y_SPECIE_H2O)/M_H2O;	/* H20 partial pressure at total pressure = 3 atm */
		q = 1./(1.0/(eps*kad*pressure_H2O)+1.0/(eps*kde)+1.0/(kd*pressure_H2O));
		dmdt_H2O = PI*pow(P_DIAM(p),2.0)*q;		/* dMp/dt */
		pml_H2O = dmdt_H2O*dt;
	
	
	}
	
	dm_spec_char[Y_SPECIE_CO2] = (pml_CO2)/dt;
  	dm_spec_char[Y_SPECIE_H2O] = (pml_H2O)/dt;
  	dm_spec_char[Y_SPECIE_O2] = (pml_O2)/dt;
}
