/******************************************************************************
 * Drying
 ******************************************************************************
 * Function:	Calculation of mass transfer from particle to gas phase  
 * 					due to evaporation of water.
 *
 * Input:	
 *	Tracked_Particle *p		pointer to particle
 *
 * Output:	
 * dm_spec_drying	rate of mass of species leaving the particle 
 * 								due to evaporation (so only water)
 *	dh_drying				heat leaving particle due to evaporation
 * evap						boolean indicating evaporation
 *****************************************************************************/
#include "Preamble.h"
#include <math.h>
#include <stdio.h>

void Drying(Tracked_Particle *p, double *dh_drying, double *dm_spec_drying,
	int *evap, Particle_Composition *pc) {
     	
	double const sigma_sb = SIGMA_SBCONST;    /* Stefan Boltzmann constant */
	double const pi = PI; 											/* ratio between the circumference of a circle to its 
																						diameter in Euclidean geometry */
	double d_p = P_DIAM(p);
	double m_p = P_MASS(p);
	double m_p_init = P_INIT_MASS(p);
	double dt = P_DT(p);
	double delta_hv_100 = DELTA_HV_100;
	double t_p = P_T(p); 																/* particle temperature */
	double t_g = C_T(P_CELL(p), P_CELL_THREAD(p)); 		/* gas temperature */
 	double k_g = C_K_L(P_CELL(p),P_CELL_THREAD(p)); 	/* thermal conductivity */
	double Pr = C_MU_T(P_CELL(p),P_CELL_THREAD(p)) * 
		   C_CP(P_CELL(p),P_CELL_THREAD(p)) / 
		   C_K_L(P_CELL(p),P_CELL_THREAD(p)); 
	double Re = P_Re(p); 	/* relative Re-number for particle */
	double epsilon_p = EPSILON_P; /* particle emissivity */
#if UDF_ENVIRONMENT == FLUENT
	cphase_state_t *c = &(p->cphase);
#endif
	double inc_rad = C_G; /* incident radiation in cell */
	/* End input */
	
	double Nu = 2. + 0.6 * sqrt(Re) * pow(Pr,1./3.); /* Ranz et Marshall*/
	double h = Nu * k_g / d_p;
	
#if UDF_ENVIRONMENT == PLUGFLOW
	Nu = 2.;
	double T_film=(t_p+T_g)/2.0;
	k_g = (7e-09*pow(T_film,3.0) - 2e-05*pow(T_film,2.0) 
					+ 0.0744*T_film + 5.436)*1e-3;
 	h = Nu*k_g/d_p;
#endif	
	
	double surf_area = pi * d_p*d_p;
	double dh_rad;
	double dh_conv;
	double dh_dry;
	double dm_h2o;

	/* Calculate heat transfered to particle with t_p = const */
	if (DPM_RADIATION_P) {
		dh_rad = surf_area * epsilon_p * 
			 (0.25*inc_rad - sigma_sb*t_p*t_p*t_p*t_p);
	} else {
		dh_rad = 0;
	}

	dh_conv = h * surf_area * (t_g - t_p);
	dh_dry = dh_conv + dh_rad;

	if (dh_dry > 0) {
		/* evaporation takes place */
		*evap = 1;
	} else {
		/* particle is beeing cooled */
		*evap = 0;
		dh_dry = 0;
/*		Message("Drying: Strange case rarely showing up.\n"
			"Particle is getting cooled while there"
			" is still water left.\n");*/		
	}
	dm_h2o = MIN(0, (-1) * dh_dry / delta_hv_100);
	
	if ((-1)*dm_h2o * dt > pc->water_remain) {
		/* limit water released to the amount left in particle */
		dm_h2o = (-1) * pc->water_remain/dt;
		dh_dry = (-1) * dm_h2o * delta_hv_100;
	}
	
	/* Begin output */
	*dh_drying = dh_dry;
	dm_spec_drying[Y_SPECIE_H2O] = dm_h2o;
	return;
}