/*****************************************************************************
 * FLUENT MACRO particle_Heat_Mass_Transfer
 *****************************************************************************
 * Function:    Switching for different submodels, call of update-functions
 * 					 for particle properties and source terms of gas-phase
 * 					 Has to be put in FLUENT as DPM_USER_LAW_1
 *
 * Input:			Particle and cell information from FLUENT
 * 
 * Output:		Source terms of heat and mass for gas phase
 * 					Modified particle information due to heat and mass transfer
 ****************************************************************************/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "Preamble.h"

int model = 0;
/* End of external definition */

DEFINE_DPM_LAW(particle_Heat_Mass_Transfer,p,coupled){ 


	double t_p = P_T(p); 															/* particle temperature */
	double t_g = C_T(P_CELL(p), P_CELL_THREAD(p));  	/* gas temperature */
	int evap = 0;    																		/* default: no evaporation of water takes place */
	static int ii;	 																		/* counter for particle iterations */
	int i;


/* Variables to pass source terms between models */
	double dh_drop = 0;
	double dm_spec_drop[NUMBER_OF_SPECIES] = { 0 };
	
	double dh_drying = 0;
	double dm_spec_drying[NUMBER_OF_SPECIES] = { 0 };
	double dh_dev = 0;
	double dm_spec_dev[NUMBER_OF_SPECIES] = { 0 };
	double dh_char = 0;
	double dm_spec_char[NUMBER_OF_SPECIES] = { 0 };
	double dh_transfer = 0;
	double m_rem_p = 0; /* difference between zero and mass of particle */

	Particle_Composition pc;

	if (P_TIME(p)-P_DT(p) < ZERO*P_DT(p)) {
		ii = 0;
		
		P_USER_REAL(p,0) = P_INIT_MASS(p) * COAL_H2O;
		P_USER_REAL(p,1) = P_INIT_MASS(p) * COAL_VOL;
 		P_USER_REAL(p,2) = P_INIT_MASS(p) * COAL_CHAR;
		
 		P_USER_REAL(p,3) = COAL_C*FRAC_C_VOL*P_INIT_MASS(p);
 		P_USER_REAL(p,4) = COAL_H*FRAC_H_VOL*P_INIT_MASS(p);
 		P_USER_REAL(p,5) = COAL_O*FRAC_O_VOL*P_INIT_MASS(p);
 		P_USER_REAL(p,6) = COAL_N*FRAC_N_VOL*P_INIT_MASS(p);
 		P_USER_REAL(p,7) = COAL_S*FRAC_S_VOL*P_INIT_MASS(p);
 		P_USER_REAL(p,8) = 0;
		
 		P_USER_REAL(p,9) = COAL_C*(1.-FRAC_C_VOL)*P_INIT_MASS(p);
 		P_USER_REAL(p,10) = COAL_H*(1.-FRAC_H_VOL)*P_INIT_MASS(p);
 		P_USER_REAL(p,11) = COAL_O*(1.-FRAC_O_VOL)*P_INIT_MASS(p);
 		P_USER_REAL(p,12) = COAL_N*(1.-FRAC_N_VOL)*P_INIT_MASS(p);
 		P_USER_REAL(p,13) = COAL_S*(1.-FRAC_S_VOL)*P_INIT_MASS(p);
 		P_USER_REAL(p,14) = 0;
		
 		P_USER_REAL(p,15) = 1;

 		P_USER_REAL(p,16) = 0; /* m_rem_p */
 		P_USER_REAL(p,17) = 0; /* h_sum */
 		P_USER_REAL(p,18) = 0; /* m_sum_spec[0] */
 		P_USER_REAL(p,19) = 0; /* m_sum_spec[1] */
 		P_USER_REAL(p,20) = 0; /* ... */
 		P_USER_REAL(p,21) = 0;
 		P_USER_REAL(p,22) = 0;
 		P_USER_REAL(p,23) = 0;
 		P_USER_REAL(p,24) = 0;
 		P_USER_REAL(p,25) = 0;
 		P_USER_REAL(p,26) = 0; 		 		
 		P_USER_REAL(p,27) = 0; /* m_sum_spec[9] */
		P_USER_REAL(p,28) = 0; /* m_sum_spec[10] */
		P_USER_REAL(p,29) = 0; /* char_burnout */
}

     /* Load values from P_USER_REAL to pc struct, this is done for 
        historical and readability reasons */
	pc.water_remain = P_USER_REAL(p,0);
	pc.vol_remain = P_USER_REAL(p,1);
	pc.char_remain = P_USER_REAL(p,2);
		
	pc.m_elem_vol[EL_C] = P_USER_REAL(p,3);
	pc.m_elem_vol[EL_H] = P_USER_REAL(p,4);
	pc.m_elem_vol[EL_O] = P_USER_REAL(p,5);
	pc.m_elem_vol[EL_N] = P_USER_REAL(p,6);
	pc.m_elem_vol[EL_S] = P_USER_REAL(p,7);
	pc.m_elem_vol[EL_H2O] = P_USER_REAL(p,8);
	
	pc.m_elem_char[EL_C] = P_USER_REAL(p,9);
	pc.m_elem_char[EL_H] = P_USER_REAL(p,10);
	pc.m_elem_char[EL_O] = P_USER_REAL(p,11);
	pc.m_elem_char[EL_N] = P_USER_REAL(p,12);
	pc.m_elem_char[EL_S] = P_USER_REAL(p,13);
	pc.m_elem_char[EL_H2O] = P_USER_REAL(p,14);
	pc.CPD_init = P_USER_REAL(p,15);

	
    ii++;
    /* Choose between parallel and serial activity of models */
    switch ( PARTICLE_TYPE ) {
    case DROPLET:
    	Droplet(p, &dh_drop, dm_spec_drop, &pc);
    	break; /* End of DROPLET */
    case COMBUSTING_PARTICLE:
    switch ( PARTICLE_MODEL_ORDER ) {
        case SERIAL_MODELS: 
            /* call subroutines to calculate source terms */
            if (P_T(p) >= T_WATER_EVAP) {
                /* Evaporation temperatur reached*/
				if (pc.water_remain > ZERO*P_INIT_MASS(p)) {
                    /* There is water left */
                    Drying(p, &dh_drying, dm_spec_drying, &evap, &pc);
					model = 1;
                } else {
                    /* No water left */
                    if (pc.vol_remain > ZERO*P_INIT_MASS(p)) {
                        /* Vol, char and ash left */
                        Devolatilization(p, &dh_dev, dm_spec_dev, &pc);
                        model = 2;
                    } else {
                        /* Char and ash left */
                        if (pc.char_remain > ZERO*P_INIT_MASS(p)) {
                       		Char_Burnout(p, &dh_char, dm_spec_char, &pc);
							
							#if NO_MODEL == 1
							/* Char-N release */
								NO_Char_Serial(p, &dh_char, dm_spec_char, pc);
							/* heterogenuose reaction on char surface */
								NO_Char_Reaction(p, &dh_char, dm_spec_char, pc);
							#endif
	
                                model = 3;
                        } else {
                            /* only ash left */
							model = 4;
						}
                    }
                }
            } else {
			/* Inert heating, temperature of particle < 373 K */
			/* dh from all models = 0, as initialized */
			/* dm from all models = 0, as initialized */
			model = 5;
            }
            break; /* end of SERIAL_MODELS */
            
        case PARALLEL_MODELS:
		
     		if (P_T(p) >= T_WATER_EVAP && pc.water_remain > ZERO*P_INIT_MASS(p)) {
        			Drying(p, &dh_drying, dm_spec_drying, &evap, &pc);
			}
       		if (pc.vol_remain > ZERO*P_INIT_MASS(p)) {
	               	Devolatilization(p, &dh_dev, dm_spec_dev, &pc);
           	}
           	
           	if (pc.char_remain > ZERO*P_INIT_MASS(p)) {
				Char_Burnout(p, &dh_char, dm_spec_char, &pc);
				#if NO_MODEL == 1
				/* Char-N release */
				NO_Char_Serial(p, &dh_char, dm_spec_char, pc);
				/* heterogenuose reaction on char surface */
				NO_Char_Reaction(p, &dh_char, dm_spec_char, pc);
				#endif
             }
               
            if (pc.water_remain < ZERO*P_INIT_MASS(p) &&
					pc.vol_remain   < ZERO*P_INIT_MASS(p) &&
					pc.char_remain  < ZERO*P_INIT_MASS(p)) {
                    /* only ash left */
    			}
            break; /* end of PARALLEL_MODELS */

            
        /* catches improper values for model_order */
        default:
            #if !RP_NODE /* Only show in host or serial process */
            Message("ERROR: The value of the variable "
                "particle_model_order"
                "is out of bounds.\n This nullifies your "
                "efforts to get reasonable results.\n"
                "Valid values are 1 for serial "
                "activity of models and 2 for parallel "
                "activity of models.\n\n");
            #endif /* !RP_NODE */
            break; 
    }
    break; /* End of COMBUSTING_PARTICLE */
    }


/* Sum values of local variables to transfer variables */
    P_USER_REAL(p,16) += m_rem_p;
    P_USER_REAL(p,17) += (-dh_drying+dh_dev+dh_char) * P_DT(p);
    /* HS -> */
    /* distribution of volatile Nitrogen and adjustment of volatile release rate */
    dm_spec_dev[Y_SPECIE_N2] = K_N2_VOL * FRAC_N_VOL * COAL_N/COAL_VOL * dm_spec_dev[Y_SPECIE_VOL];
    dm_spec_dev[Y_SPECIE_CO2] = dm_spec_dev[Y_SPECIE_VOL] * K_CO2_VOL;
    dm_spec_dev[Y_SPECIE_CO] = dm_spec_dev[Y_SPECIE_VOL] * K_CO_VOL;
	dm_spec_dev[Y_SPECIE_VOL] = dm_spec_dev[Y_SPECIE_VOL] - dm_spec_dev[Y_SPECIE_N2] - dm_spec_dev[Y_SPECIE_CO2] - dm_spec_dev[Y_SPECIE_CO] ;

    for (i = 0; i < NUMBER_OF_SPECIES; i++) {
	P_USER_REAL(p,18+i) += (dm_spec_drying[i]+dm_spec_dev[i]+dm_spec_char[i]) 
	                                            * P_DT(p);
    }
    P_USER_REAL(p,29) += (dm_spec_char[Y_SPECIE_CO2]*M_C/M_CO2+dm_spec_char[Y_SPECIE_CO]*M_C/M_CO)*P_DT(p);

    /* Particle updates 
       WARNING! Changing the order of these leads you directly to hell! */
	 /*1st: Update particle temperature, 2nd: Update particle density and diameter, 3rd: Update particle mass*/
    Update_Temperature_P(p, dh_dev, dh_char, &dh_transfer, evap);
    Update_Rho_Diam_P(p, dm_spec_drying, dm_spec_dev, dm_spec_char);
    Update_Mass_P(p, dm_spec_drying, dm_spec_dev, dm_spec_char, \
		  m_rem_p, &pc);
		  
     /* Load values from pc struct back to P_USER_REAL, this is done for 
        historical and readability reasons */
	P_USER_REAL(p,0) = pc.water_remain;
	P_USER_REAL(p,1) = pc.vol_remain;
	P_USER_REAL(p,2) = pc.char_remain;
                   		
	P_USER_REAL(p,3) = pc.m_elem_vol[EL_C];
	P_USER_REAL(p,4) = pc.m_elem_vol[EL_H];
	P_USER_REAL(p,5) = pc.m_elem_vol[EL_O];
	P_USER_REAL(p,6) = pc.m_elem_vol[EL_N];
	P_USER_REAL(p,7) = pc.m_elem_vol[EL_S];
	P_USER_REAL(p,8) = pc.m_elem_vol[EL_H2O];
                   	
	P_USER_REAL(p,9) = pc.m_elem_char[EL_C];
	P_USER_REAL(p,10) = pc.m_elem_char[EL_H];
	P_USER_REAL(p,11) = pc.m_elem_char[EL_O];
	P_USER_REAL(p,12) = pc.m_elem_char[EL_N];
	P_USER_REAL(p,13) = pc.m_elem_char[EL_S];
	P_USER_REAL(p,14) = pc.m_elem_char[EL_H2O];
	P_USER_REAL(p,15) = pc.CPD_init;

}