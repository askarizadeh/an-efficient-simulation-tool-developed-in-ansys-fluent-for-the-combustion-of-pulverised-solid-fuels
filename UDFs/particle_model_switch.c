/******************************************************************************
 * FLUENT MACRO Particle_Model_Switch
 ******************************************************************************
 * Function:
 * Replacement for FLUENT particle model switch. As the switching is done in
 * particle_Heat_Mass_Transfer this basically does nothing else than calling
 * that UDF.
 *
 * Usage:
 * Has to be hooked as DPM_SWITCH into FLUENT, allways calls DPM_LAW_USER_1 
 * which has to be DPM_LAW particle_Heat_Mass_Transfer.
 *
 * Input:
 * Tracked_Particle  *p    Pointer to structure which contains all information 
 *                         				regarding the particle
 *
 * Output:
 * int   P_CURRENT_LAW(p)  MACRO for index of active DPM model
 *****************************************************************************/

#include "udf.h"

DEFINE_DPM_SWITCH(dpm_switch,p,coupled)
{
	P_CURRENT_LAW(p) = DPM_LAW_USER_1;
}
