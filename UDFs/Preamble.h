/*****************************************************************************
 * Environment to run the particle UDF
 ****************************************************************************/
/* Options for UDF_ENVIRONMENT */
#define FLUENT   1
#define PLUGFLOW 0


#include "user_input.h"

#if UDF_ENVIRONMENT == FLUENT
  #include "udf.h"
  #include "surf.h"
#elif UDF_ENVIRONMENT == PLUGFLOW
  #include "plugflow.h"
  #include "plugflow_Tools.h"
#endif

#define PHYSCON 2.5e-12		/* PHYSCON assumes O2 partial pressure in Pascals */
#define PCELL 1.013e05  		/* pressure 1 atm, in Pa */

/******************************************************************************
 * Model definitions
 *****************************************************************************/

/* defines wich particle type to use: */
  #define DROPLET		0
  #define COMBUSTING_PARTICLE	1


/* defines the activity of particle models: */
  #define SERIAL_MODELS		0
  #define PARALLEL_MODELS	1


/* defines the operating conditions: */
  #define AIR_CONDITIONS	0
  #define OXYCOAL_CONDITIONS	1

/* defines the devolatilization model to use: */
  #define ONE_REACTION	0
  #define TWO_REACTIONS	1
  #define CPD		2
  #define CPD_NLG	3

/* defines the char model to use: */
  #define CBK 		0					/* Hurt, 1998 */
  #define FIRST_ORDER	1		/* Field, 1969 */
  #define INTRINSIC	2			/* to be tested !!*/
  #define LANGMUIR	3			/* to be tested !!*/

/* Values for phi2 (ratio between CO and CO2) */
  #define PHI2_CO2	0					/* CO2 only */
  #define PHI2_CO	1					/* CO only */
  #define PHI2_ARTHUR	2		/* Arthur */
  #define PHI2_HURT	3				/* Hurt, 1998 */

/* defines wether	1 - diameter
 * 			2 - density or 
 * 			3 - both change during mass release */
  #define RDC_DIAMETER 1
  #define RDC_RHO      2
  #define RDC_RHO_DIAM 3

#if UDF_ENVIRONMENT == FLUENT
  #define P_Re(p) p->Re
  #if DPM_RADIATION_P
    #define C_G c->G
  #else
    #define C_G 0
  #endif
#endif


/* for the CPD devolatilization model */
/* the following 5 model parameters are calculated in a pre-processor, 
 * using ultimate coal analysis as input */
#define p0_cpd       0.63			/* Initial fraction of itact bridges */
#define sigp1_cpd    3.9			/* coordination number */
#define mw1_cpd      277.		/* average molecular weight of cluster (including side chains) */
#define mdel_cpd     40.0		/* average molecular wight of side chains */
#define c0_cpd       0.15			/* initial fraction of char bridges */



/* Molar masses of elements and species */
#define M_O 15.9994
#define M_C 12.0107
#define M_H 1.00794
#define M_N 14.0067
#define M_S 32.065
#define M_H2  (2*M_H)
#define M_O2  (2*M_O)
#define M_H2O (M_O+2*M_H)
#define M_CO (M_O+M_C)
#define M_CO2 (M_C+2*M_O)
#define M_SO2 (M_S+2*M_O)
#define M_CH4 16.042 
#define M_VOL 28.65

#define M_N2 (2. * M_N) 
#define M_HCN (M_H + M_C + M_N)
#define M_NO (M_N + M_O)
#define M_NH3 (M_N + 3. * M_H)

#define P_T_REF 298.15						/* Reference temperature for enthalpy */
#define T_WATER_EVAP 373.15		/* Evaporation temperature in Kelvin */
#define PI 3.1415926535897931
#define SIGMA_SBCONST 5.67e-8 	/* W/m2K4, Stephan-Bolzman constant */
#define DELTA_HV_100 2256700.0	/* J/kg, Heat of evaporation @ 100�C, 1 bar*/

#define HOF_CO -110500000.0  		/* enthalpy of formation of CO  in J/kmol */
#define HOF_CO2 -393522000.0   	/* enthalpy of formation of CO2 in J/kmol */
#define HOF_H2O -241838000.0 		/* enthalpy of formation of H2O in J/kmol */

#define HOF_HCN -135149200.0  	/* enthalpy of formation of CO  in J/kmol */
#define HOF_NH3 -459157400.0  	/* enthalpy of formation of CO  in J/kmol */
#define HOF_NO 90288480.0  			/* enthalpy of formation of CO  in J/kmol */
#define HOF_N 153180.3  					/* enthalpy of formation of CO  in J/kmol */

/* Define value to compare a double with instead of a exact 0 */
#define ZERO	1e-15


/******************************************************************************
 * struct definitions
 *****************************************************************************/
/* struct which contains the actual mass of components and elements
   of the particle */
typedef struct particle_composition_struct {
	double water_remain;
	double vol_remain;
	double char_remain;
	double m_elem_vol[NUMBER_OF_ELEMENTS];
	double m_elem_char[NUMBER_OF_ELEMENTS];
	int CPD_init;
} Particle_Composition;
	
/******************************************************************************
 * Function prototype definition
 *****************************************************************************/
void Drying(Tracked_Particle *p, double *dh_drying, double *dm_spec_drying, \
            int *evap, Particle_Composition *pc);
void Update_Rho_Diam_P(Tracked_Particle *p, double *dm_spec_drying, \
		       double *dm_spec_dev, double *dm_spec_char);
void Update_Temperature_P(Tracked_Particle *p, double dh_dev, double dh_char,\
			  double *dh_transfer, int evap); 
void Update_Mass_P(Tracked_Particle *p, double *dm_spec_drying, \
		   double *dm_spec_dev, double *dm_spec_char, \
		   double m_rem_p, Particle_Composition *pc);
void Devolatilization(Tracked_Particle *p, double *dh_dev, \
                      double *dm_spec_dev, Particle_Composition *pc);
void Char_Burnout(Tracked_Particle *p, double *dh_char, double *dm_spec_char, \
                  Particle_Composition *pc);
void First_Order(Tracked_Particle *p, double *dh_char, double *dm_spec_char, \
                 Particle_Composition *pc);
void Carbon_Burnout_Kinetic(Tracked_Particle *p, double *dh_char, \
                            double *dm_spec_char, Particle_Composition *pc);
void Langmuir(Tracked_Particle *p, double *dh_char, double *dm_spec_char, \
              Particle_Composition *pc);
void Intrinsic(Tracked_Particle *p, double *dh_char, double *dm_spec_char, \
               Particle_Composition *pc);
void One_Reaction(Tracked_Particle *p, double *dh_dev, double *dm_spec_dev, \
                  Particle_Composition *pc);
void Two_Reactions(Tracked_Particle *p, double *dh_dev, double *dm_spec_dev, \
                   Particle_Composition *pc);
/*void CPD_Model(Tracked_Particle *p, double *dh_dev, double *dm_spec_dev, \
               Particle_Composition *pc);*/
void Droplet(Tracked_Particle *p, double *dh_drop, \
                      double *dm_spec_drop, Particle_Composition *pc);

/* collection of tools from particle_tools.c */
double Specific_Heat_P(Tracked_Particle *p);
double Phi2(int method, double t_p);