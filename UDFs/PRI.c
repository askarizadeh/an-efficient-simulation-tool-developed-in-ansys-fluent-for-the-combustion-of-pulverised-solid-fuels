/******************************************************************
PRI - Particle Radiation Interaction
*******************************************************************/

#include "PRI.h"
#include "udf.h"

/*	Implementation of calculation of Scattering functions. */
double PRI_ScatCoeff(double xi, double D_p, double rho_p, double Qsca){
	
	double particle_scat_coeff;
	double N_PV;			

	N_PV = xi/rho_p;
	particle_scat_coeff = N_PV * Qsca * 6/(4*D_p) ;
	
	return particle_scat_coeff;
}

double PRI_AbsCoeff(double xi, double D_p, double rho_p, double Qabs){
	
	double particle_abs_coeff;
	double N_PV;			/* 	Number of particles per Unit Volume [-] 		*/	
		
	N_PV = xi/rho_p;
	particle_abs_coeff = N_PV * Qabs * 6/(4*D_p);
	
	return particle_abs_coeff;
}