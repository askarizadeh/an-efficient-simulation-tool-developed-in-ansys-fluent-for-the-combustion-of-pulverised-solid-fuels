/******************************************************************************************
 * Devolatilization
 /******************************************************************************************
 * Function:    
 * Function to redirect to the active devolatilization model.
 * All the devolatilization models have to be placed here
 *
 * Input:       
 * Tracked_Particle 				*p     					pointer to particle structure p
 * double    							*dh_dev       		heat released by devolatilization
 * double    							*dm_spec_dev  	mass of species released by devolatilization
 * int const  							DEV_MODEL    	active model, defined in coalprop.h
 * Particle_Composition 	pc		 				structure containing particle composition
 *
 * Output:      
 * Tracked_Particle 				*p     					pointer to particle structure p
 * double 								*dh_dev          	heat released by devolatilization
 * double 								*dm_spec_dev   mass of species released by devolatilization
 *****************************************************************************************/

#include "Preamble.h"
#include <stdio.h>

void Devolatilization(Tracked_Particle *p, double *dh_dev, double *dm_spec_dev,
                      Particle_Composition *pc) {

	/* chose from the various models */
	switch (DEVOLATILIZATION_MODEL) {
		case ONE_REACTION:
			One_Reaction(p, dh_dev, dm_spec_dev, pc);
			break;
			
		case TWO_REACTIONS:
			Two_Reactions(p, dh_dev, dm_spec_dev, pc);
			break;
			
		case CPD:
			CPD_Model(p, dh_dev, dm_spec_dev, pc);
			break;
			
		default:
			#if !RP_NODE /* Only show in host or serial process */
				Message("ERROR: The value of the variable "
				"DEVOLATILIZATION_MODEL "
				"is out of bounds.\n This nullifies your "
				"efforts to get reasonable results.\n\n");
			#endif /* !RP_NODE */
 			break;
	}
	return;
}