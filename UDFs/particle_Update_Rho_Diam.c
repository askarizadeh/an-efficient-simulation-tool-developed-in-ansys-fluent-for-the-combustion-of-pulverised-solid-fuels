/******************************************************************************
 * Update_Rho_Diam
 ******************************************************************************
 * Function:	Keeps track of the diameter and density of the particle
 *
 * Input:	
 * Tracked_Particle  *p       							Pointer to structure p
 * double  					*dm_spec_drying[]  	species mass exchange from evaporation
 * double  					*dm_spec_dev[]     	species mass exchange from devolatilisation 
 * double  					*dm_spec_char[]    	species mass exchange from char burnout
 * double   					P_MASS(p)         		particle mass
 * double   					P_DIAM(p)         		particle diameter
 * double   					P_DT(p)           			duration of particle time step
 * int const 				NUMBER_OF_SPECIES 
 *
 * Output:	P_RHO(p)		density of particle
 * 				P_DIAM(p)	diameter of particle
 *****************************************************************************/
#include <math.h>
#include <stdio.h>
#include "Preamble.h"

void Update_Rho_Diam_P(Tracked_Particle *p, double *dm_spec_drying,
                       double *dm_spec_dev, double *dm_spec_char) {

	double d_p = P_DIAM(p); 		/* particle diameter */
	double m_p = P_MASS(p); 		/* particle mass */
	double rho_p = P_RHO(p); 	/* density of particle */
	double dt = P_DT(p);    			/* duration of particle time step */
	/* End input */

	double dm_sum = 0;
	double dm_sum_char = 0;
	int i;

	/* Total mass leaving the particle */
	for (i = 0; i < NUMBER_OF_SPECIES; i++) {
		dm_sum += dm_spec_drying[i] + dm_spec_dev[i] + dm_spec_char[i];
		dm_sum_char += dm_spec_char[i];
	}
	
	/* Evaporation and devolatilisation lead to change of density,
	   char burnout leads to change of diameter */
	if (d_p) {
		/* change of density, diameter is constant */
		rho_p = ((m_p+dm_sum_char*dt)*6.) / (PI * d_p*d_p*d_p);
	}
	if (rho_p) {
		/* change of diameter, density is constant */
		d_p = pow((m_p+dm_sum*dt)*6./(PI*rho_p),1./3.);
	}
	
	/* Begin output */
	P_RHO(p) = rho_p;
	P_DIAM(p) = d_p;
	return;
}