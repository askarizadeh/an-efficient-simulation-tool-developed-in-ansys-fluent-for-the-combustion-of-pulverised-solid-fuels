/***************************************************************************************
 * 													Char Burnout
/***************************************************************************************
 * Function:    
 * Function to redirect to the active char burnout model. All the char burnout 
 * models have to be placed here.
 *              
 * Input:       
 * Tracked_Particle  *p     						pointer to particle structure p
 * double    				*dh_char       			heat released by char burnout
 * double    				*dm_spec_char  	mass of species released by char burnout 
 * int const  				CHAR_MODEL 		active model, defined in solid_fuel_properties.h
 *
 * Output:
 * Tracked_Particle  *p     						pointer to particle structure p
 * double  					*dh_char         		heat released by char burnout
 * double  					*dm_spec_char    	mass of species released by char burnout 
***************************************************************************************/
#include <stdio.h>
#include "Preamble.h"

void Char_Burnout(Tracked_Particle *p, double *dh_char, double *dm_spec_char,
                  Particle_Composition *pc) { 
	/* chose from the various models */
	switch (CHAR_MODEL) {
		case CBK:
			Carbon_Burnout_Kinetic(p, dh_char, dm_spec_char, pc);
			break;
			
		case FIRST_ORDER:
			First_Order(p, dh_char, dm_spec_char, pc);
			break;
			
		case INTRINSIC:
			Intrinsic(p, dh_char, dm_spec_char, pc);
			break;
			
		case LANGMUIR:
			Langmuir(p, dh_char, dm_spec_char, pc);
			break;
			
		default:
			#if !RP_NODE /* Only show in host or serial process */
				Message("ERROR: The value of the variable "
				"CHAR_MODEL "
				"is out of bounds.\n This nullifies your "
				"efforts to get reasonable results.\n\n");
			#endif /* !RP_NODE */
 			break;
	}	
	return;
}
