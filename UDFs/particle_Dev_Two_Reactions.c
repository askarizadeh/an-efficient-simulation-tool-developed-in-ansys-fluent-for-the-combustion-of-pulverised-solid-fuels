/******************************************************************************
 * Two Reactions Devolatilisation model
 ******************************************************************************
 * Function:    provides the two reactions devolatilisation model.
 *              
 * Input:      
 * Tracked_Particle *p		pointer to particle
 * dh_dev							heat released by devolatilisation
 * dm_spec_dev					mass of species released by devolatilisation
 * Particle_Composition pc
 
 * Output:      
 * dh_dev				heat released by devolatilisation (=0)
 * dm_spec_dev		vector of species masses released by devolatilisation
 *****************************************************************************/
#include <math.h>
#include <stdio.h>
#include "Preamble.h"

void Two_Reactions(Tracked_Particle *p, double *dh_dev, double *dm_spec_dev,
                   Particle_Composition *pc) {
	
	double Rgas = UNIVERSAL_GAS_CONSTANT;
	double dev_a1 = DEV_A1; 							/* preexponential factor */
	double dev_e1 = DEV_E1; 							/* activation energy */
	double dev_a2 = DEV_A2; 							/* preexponential factor */
	double dev_e2 = DEV_E2; 							/* activation energy */
	double dev_alpha1 = DEV_ALPHA1; 		/*  */
	double dev_alpha2 = DEV_ALPHA2; 		/*  */
	double t_p = P_T(p); 									/* particle temperature */
	double dt = P_DT(p); 									/* duration of particle time step */
	double m_p = P_MASS(p); 							/* particle mass */
	double m_p_init = P_INIT_MASS(p); 		/* particle initialisation mass */
	double p_vff = P_VFF(p); 							/* nasty, undocumented FLUENT stuff */
	/* End input */
	
	double dm_dev; /* mass release rate of volatiles */	
	double kin_rate1, kin_rate2; /* kinetic rates */
     
	kin_rate1 = dev_a1*exp(-dev_e1/(Rgas*t_p));
	kin_rate2 = dev_a2*exp(-dev_e2/(Rgas*t_p));
	
	/* This is what FLUENT does */
	p_vff = p_vff * exp(-(kin_rate1+kin_rate2)*dt);
	
	dm_dev = (-1) * (dev_alpha1*kin_rate1+dev_alpha2*kin_rate2) * p_vff * m_p_init*(COAL_VOL+COAL_CHAR);
  
	/* if 99% of volatiles should have been evaporated, release remaining */
	/* In fact in this very moment it is released less (because of alpha1
	   and alpha2, but if these were equal to 1, we would have released 99%, so, 
	   to shorten things, we release the remaining part. 
	   Check if we released too much and fix this if necessary.*/
	if (p_vff < 0.01 || (-1)*dm_dev*dt > pc->vol_remain) {
		dm_dev = (-1) * pc->vol_remain / dt;
	}
	/* Begin output */
	P_VFF(p) = p_vff;
	dm_spec_dev[Y_SPECIE_VOL] = dm_dev;
}
