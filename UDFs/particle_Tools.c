#include <stdio.h>
#include <math.h>
#include "Preamble.h"

double Phi2(int method, double t_p){
	double phi2 = 1., pfactor;
	switch ( method ){
		case PHI2_CO2:
			phi2 = 1.;
			break;
		case PHI2_CO:
			phi2 = 2.;
			break;
		case PHI2_ARTHUR:
			pfactor = 2500.0*exp(-6240./t_p);
			phi2 = (2.*pfactor+2.)/(pfactor+2.);
			break;
		case PHI2_HURT:
			pfactor=3.0e8*exp(-251160./(t_p*8.314));
			phi2 = (2.*pfactor+2.)/(pfactor+2.);
			break;
		default:
			#if !RP_NODE /* Only show in host or serial process */
			Message("ERROR: The method chosen for phi2 is invalid!\n");
			#endif /* !RP_NODE */
			break;
	}
	return phi2;
}

double Specific_Heat_P(Tracked_Particle *p)
{
	/* put different models for cp here */
	double cp_p = DPM_SPECIFIC_HEAT(p,P_T(p));
	return cp_p;
}
