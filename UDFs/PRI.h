/*	Defines Particle Radiation Interaction stuff	*/
#include "udf.h"

#define N_PRI_UDMI	29
#define PI 3.14159265359

double PRI_ScatCoeff(double xi, double D_p, double rho_p, double Qsca);
double PRI_AbsCoeff(double xi, double D_p, double rho_p, double Qabs);

