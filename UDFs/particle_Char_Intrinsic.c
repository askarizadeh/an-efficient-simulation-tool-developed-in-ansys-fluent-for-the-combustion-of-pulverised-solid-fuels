/******************************************************************************
 * Intrinsic
 ******************************************************************************
 * Function:    provides the Intrinsic char burnout model.
 *              
 * Input:       	
 * Tracked_Particle  *p    pointer to particle structure p
 * dh_char							heat released by char burnout
 * dm_spec_char				mass of species released by char burnout
 *
 * Output:      	
 * dh_char				heat released by char burnout
 * dm_spec_char	mass of species released by char burnout 
 *****************************************************************************/
#include "Preamble.h"
#include <math.h>
#include <stdio.h>

void Intrinsic(Tracked_Particle *p, double *dh_char, double *dm_spec_char,
						Particle_Composition *pc) {
							
	double dt = P_DT(p)	; 																															/* particle time step */	
	double T_p = P_T(p)	; 																															/* particle temperature */
	double d_p = P_DIAM(p) ; 																														/* particle diameter */
	double m_p_init = P_INIT_MASS(p); 																									/* particle mass at initialisation */
	double Sb = MATERIAL_PROP(P_MATERIAL(p), PROP_burn_stoich) ;
	double T_g = C_T(P_CELL(p),P_CELL_THREAD(p)) ; 																		/* gas temperature in the cell where the particle is located [K] 	*/
	double rho_g = C_R(P_CELL(p),P_CELL_THREAD(p)) ; 																	/* gas density [kg/m^3] 	*/
	double mfrac_O2 = C_YI(P_CELL(p), P_CELL_THREAD(p), Y_SPECIE_O2) ;								/* mass fraction of O2 		*/
	double mfrac_CO2 = C_YI(P_CELL(p), P_CELL_THREAD(p), Y_SPECIE_CO2) ;							/* mass fraction of CO2 	*/
	double mfrac_H2O = C_YI(P_CELL(p), P_CELL_THREAD(p), Y_SPECIE_H2O) ;							/* mass fraction of H2O 	*/
	double f_ash = COAL_ASH;    																													/* fraction of ash in particle*/
	double dmdt_O2 = 0.0, dmdt_CO2, dmdt_H2O, rate1, rate2, rate1_CO2, rate2_CO2, rate1_H2O, rate2_H2O ;
	
	double dm_O2 = 0; 		/* mass release rates for O2 */
	double dm_CO2 = 0; 	/* mass release rates for CO2 */
	double dm_H2O = 0; 	/* mass release rates for H2O */
	
	double Rgas = UNIVERSAL_GAS_CONSTANT ;
	double Eff_Diff_Coef, Kn_Diff_Coef, Diff_Rate_Coef ;
	double Part_Pressure_O2, Part_Pressure_CO2, Part_Pressure_H2O ;    	/* partial pressure of gas species contributing to char reaction (Pa) */
	double C_O2, C_CO2, C_H2O;    																			/* concentrations of gas species contributing to char reactions (kg/m3) */
	
	double pml_O2, pml_CO2, pml_H2O, m_char_sum, m_p_new, mass_rem_fraction ;
	pml_CO2 = 0.0 ;
	pml_H2O = 0.0 ;		
	pml_O2 = 0.0 ;
	
	double Hchar_O2, Hchar_CO2, Hchar_H2O ;													/* heat from heterogenous reaction J/kg */
	double chfac ;

    double char_c1_O2 = CHAR_C1_O2 ;  																/* 5e-12 */
    double char_c2_O2 = CHAR_C2_O2 ;  																/* 0.031 */
    double char_e2_O2 = CHAR_E2_O2 ;																/* 1.794e+8 J/kmole*/
	double porosity = 0.5 ; 																						/* POROSITY */  
	double pore_radius = 6e-8 ; 																				/* PORE_RADIUS						 	6e-8 [m] */
	double internal_area = 300000 ; 																		/* INTERNAL_AREA				 	300000 [m^2/kg] */
	double tortuosity = 1.41 ; 																					/* MAX(1.e-10,TORTUOSITY);  1.41 */
	double thiele_modulus_O2, thiele_modulus_CO2, thiele_modulus_H2O ;
	double eff_coeff_O2, eff_coeff_CO2, eff_coeff_H2O ; 										/* Effectiveness factor of reactive species with char, Equation 22.9-79  */

	double char_c1_CO2 = CHAR_C21_CO2 ;
    double char_c2_CO2 = CHAR_C22_CO2 ;
    double char_e2_CO2 = CHAR_E22_CO2 ;
	
	double char_c1_H2O = CHAR_C21_H2O ;
    double char_c2_H2O = CHAR_C22_H2O ;
    double char_e2_H2O = CHAR_E22_H2O ;

	/* alpha = 0 */
    double phi2 = Phi2(PHI2, T_p) ; 																	/* Look at the "particle tool" udf to see the correlations for the calculation of Phi2 */
	chfac = MATERIAL_PROP(P_MATERIAL(p) , PROP_hreact_frac) ;
	/* Hchar calculates the heat of heterogeneous reaction as a function of product species (CO and/or CO2) */
	Hchar_O2 = (-HOF_CO/M_C)*(2.-2./phi2)+(-HOF_CO2/M_C)*(2./phi2-1);		/* =   9.20833e6 J/kgCarbon */	/* HOF_CO and HOF_CO2 are defined in the header file "soild_fuel_properties.h" */
	Hchar_CO2 = HOF_CO2/M_C - 2. * HOF_CO/M_C;													/* = -14.37683e6 J/kgCarbon */
	Hchar_H2O = HOF_H2O/M_C - HOF_CO/M_C;
	
	/* =========== O2 reaction ============ */
	C_O2 = rho_g * mfrac_O2 ;
	rate1 = char_c1_O2 * pow(0.5 * (MAX(1.0 , T_p) + T_g) , 0.75) / d_p ; 	/* Equation 16.166 Theory Guide */
	rate2 = char_c2_O2 * exp(-char_e2_O2 / (Rgas * MAX(1.0 , T_p))) ;     	/* Equation 16.169 Theory Guide */
    
	if (C_O2 > 1.e-25) 
	{
		/* Knudsen diffusion coefficient, Equation 16.168 Theory Guide */
		Kn_Diff_Coef = 97.0 * pore_radius * sqrt(T_p/M_O2);
		/* Effective diffusion coefficient in the particle pores, EquationEquation 16.166 Theory Guide */
		Eff_Diff_Coef = porosity / (tortuosity*tortuosity) * (char_c1_O2 * Kn_Diff_Coef / (char_c1_O2 + Kn_Diff_Coef) ) ;
            
		Part_Pressure_O2 = rho_g * Rgas * T_g * mfrac_O2 / M_O2 ;
		/* Thiele modulus, Equation 16.165 */
		thiele_modulus_O2 = 0.5 * d_p * sqrt(Sb * P_RHO(p) * internal_area * rate2 * Part_Pressure_O2 / (Eff_Diff_Coef * C_O2)) ;
		thiele_modulus_O2 = MAX(0.01, thiele_modulus_O2) ;
		/* Effectiveness factor, Equation 16.164 */
		eff_coeff_O2 = 3.0/(thiele_modulus_O2*thiele_modulus_O2) * (thiele_modulus_O2/tanh(thiele_modulus_O2) - 1.0) ;
		eff_coeff_O2 = MAX(0.0 , MIN(1.0 , eff_coeff_O2)) ;
	    	
		if (eff_coeff_O2 <= 1.0e-5) 
		{
			dmdt_O2 = -PI * pow(d_p , 2) * Part_Pressure_O2 * rate1 ;
		}
		else 
		{
			rate2 = rate2 * eff_coeff_O2 * internal_area * P_RHO(p) * d_p / 6.0 ;
			dmdt_O2 = -PI * pow(d_p , 2) * Part_Pressure_O2 * rate1 * rate2 / (rate1 + rate2) ;
		}		
	} else {
		dmdt_O2 = 0.0 ;
	}
	/*printf("eff_coeff: %e rate1: %e rate2: %e dmdt_O2: %e\n",eff_coeff,rate1,rate2,dmdt_O2);*/
	pml_O2 = dmdt_O2 * dt ;
	
	if (NUMBER_CHAR_REACTIONS == 3) 
	{
		C_CO2 = rho_g * mfrac_CO2 ;
		rate1_CO2 = char_c1_CO2 * pow(0.5 * (MAX(1.0 , T_p) + T_g) , 0.75) / d_p ; 		/* Equation 16.166 Theory Guide */
		rate2_CO2 = char_c2_CO2 * exp(-char_e2_CO2 / (Rgas * MAX(1.0 , T_p))) ;    	/* Equation 16.169 Theory Guide */
		if (C_CO2 > 1.e-25) 
		{
			Part_Pressure_CO2 = rho_g * Rgas * T_g * mfrac_CO2 / M_CO2 ;
			thiele_modulus_CO2 = 0.5 * d_p * sqrt(Sb * P_RHO(p) * internal_area * rate2 * Part_Pressure_CO2/(Eff_Diff_Coef * C_CO2));
			thiele_modulus_CO2 = MAX(0.01 , thiele_modulus_CO2) ;
			eff_coeff_CO2 = 3.0 / pow(thiele_modulus_CO2 , 2) * (thiele_modulus_CO2/tanh(thiele_modulus_CO2) - 1.0) ;
			eff_coeff_CO2 = MAX(0.0 , MIN(1.0 , eff_coeff_CO2)) ;
		
			if (eff_coeff_CO2 <= 1.0e-5) 
			{
				dmdt_CO2 = -PI * pow(d_p , 2) * Part_Pressure_CO2 * rate1_CO2 ;
			}
			else 
			{
				rate2_CO2 = rate2_CO2 * eff_coeff_CO2 * internal_area * P_RHO(p) * d_p / 6.0 ;
				dmdt_CO2 = -PI * pow(d_p , 2) * Part_Pressure_CO2 * rate1_CO2 * rate2_CO2 / (rate1_CO2 + rate2_CO2) ;
			}    
		}
        	else {
			dmdt_CO2 = 0.;
		}
		pml_CO2 = -dmdt_CO2 * dt ;
				
		C_H2O = rho_g * mfrac_H2O ;
		rate1_H2O = char_c1_H2O * pow(0.5 * (MAX(1.0 , T_p) + T_g) , 0.75) / d_p ; 		/* Equation 16.166 Theory Guide */
		rate2_H2O = char_c2_H2O * exp(-char_e2_H2O / (Rgas * MAX(1.0 , T_p))) ;    	/* Equation 16.169 Theory Guide */
		
		if (C_H2O > 1.e-25) 
		{
			Part_Pressure_H2O = rho_g * Rgas * T_g * mfrac_H2O / M_H2O ;
			thiele_modulus_H2O = 0.5 * d_p * sqrt(Sb * P_RHO(p) * internal_area * rate2 * Part_Pressure_H2O / (Eff_Diff_Coef * C_H2O)) ;
			thiele_modulus_H2O = MAX(0.01 , thiele_modulus_H2O) ;
			eff_coeff_H2O = 3.0 / pow(thiele_modulus_H2O , 2) * (thiele_modulus_H2O / tanh(thiele_modulus_H2O) - 1.0) ;
			eff_coeff_H2O = MAX(0.0 , MIN(1.0 , eff_coeff_H2O)) ;
		
			if (eff_coeff_H2O <= 1.0e-5) 
			{
				dmdt_H2O = -PI * pow(d_p , 2) * Part_Pressure_H2O * rate1_H2O ;
			}
			else 
			{
				rate2_H2O = rate2_H2O * eff_coeff_H2O * internal_area * P_RHO(p) * d_p / 6.0 ;
				dmdt_H2O = -PI * pow(d_p , 2) * Part_Pressure_H2O * rate1_H2O * rate2_H2O / (rate1_H2O + rate2_H2O) ;
			}    
		}
        	else {
			dmdt_H2O = 0.;
		}
		pml_H2O = -dmdt_H2O * dt ;
	}	
	m_char_sum = (dm_O2 + dm_CO2 + dm_H2O)*dt; /* total char mass released */


/* Check if the mass released from the last time step is overpredicted. 
   If yes, a linear interpolation is made.*/
	if ((-1)*m_char_sum > pc->char_remain)
	{
		m_p_new = m_p_init*f_ash;	
		
		mass_rem_fraction = (-1)*pc->char_remain/m_char_sum;

		dm_O2  = dm_O2  * mass_rem_fraction;
		dm_CO2 = dm_CO2 * mass_rem_fraction;
		dm_H2O = dm_H2O * mass_rem_fraction; 
	}

	dm_spec_char[Y_SPECIE_CO] += (2. - 2./phi2) * M_CO/M_C * dmdt_O2 ;
	dm_spec_char[Y_SPECIE_O2] -= 1. / phi2 * M_O2 / M_C * dmdt_O2 ;
	dm_spec_char[Y_SPECIE_CO2] += (2. / phi2 - 1.) * M_CO2 / M_C * dmdt_O2 ;
	
	if (NUMBER_CHAR_REACTIONS == 3) 
	{
		/* Mass source due to reaction of char with CO2 */	
		dm_spec_char[Y_SPECIE_CO] += 2. * M_CO / M_C * dmdt_CO2 ;
		dm_spec_char[Y_SPECIE_CO2] -= M_CO2 / M_C * dmdt_CO2 ;	

		/* Mass source due to reaction of char with H2O */
		dm_spec_char[Y_SPECIE_CO] += M_CO / M_C * dmdt_H2O ;
		dm_spec_char[Y_SPECIE_H2O] -= M_H2O/M_C * dmdt_H2O ;
	}
	*dh_char = -chfac*(Hchar_O2*dm_O2 + Hchar_CO2*dm_CO2 + Hchar_H2O*dm_H2O);
}