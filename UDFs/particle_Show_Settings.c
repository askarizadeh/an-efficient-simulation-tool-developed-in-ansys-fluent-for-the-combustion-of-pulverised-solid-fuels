/******************************************************************************
 * particle_Show_Settings
 ******************************************************************************
 * Function:    Displays UDF-settings from Preamble.h
 *
 * Input:       -
 *
 * Output:     -
 *****************************************************************************/

#include "Preamble.h"

DEFINE_ON_DEMAND(Display_Settings)
{
	/* print Preamble.h settings */
	Message("\nModel settings from Preamble.h:\n");
	Message("\nUDF_ENVIRONMENT %d\n", UDF_ENVIRONMENT);
	Message("PARTICLE_MODEL_ORDER %d\n", PARTICLE_MODEL_ORDER);
	Message("OPERATING_CONDITIONS %d\n", OPERATING_CONDITIONS);
	Message("DEVOLATILIZATION_MODEL %d\n", DEVOLATILIZATION_MODEL);
	Message("CHAR_MODEL %d\n", CHAR_MODEL);
	Message("NUMBER_CHAR_REACTIONS %d\n", NUMBER_CHAR_REACTIONS);
	Message("DPM_RADIATION_P %d\n", DPM_RADIATION_P);
	Message("NUMBER_OF_SPECIES %d\n", NUMBER_OF_SPECIES);
	Message("\nOrder of species in mixture material\n");
	#ifdef Y_SPECIE_VOL
		Message("	Y_SPECIE_VOL %d\n", Y_SPECIE_VOL);
	#endif
	#ifdef Y_SPECIE_O2
		Message("	Y_SPECIE_O2  %d\n", Y_SPECIE_O2);
	#endif
	#ifdef Y_SPECIE_CO2
		Message("	Y_SPECIE_CO2 %d\n", Y_SPECIE_CO2);
	#endif
	#ifdef Y_SPECIE_H2O
		Message("	Y_SPECIE_H2O %d\n", Y_SPECIE_H2O);
	#endif
	#ifdef Y_SPECIE_CO
		Message("	Y_SPECIE_CO  %d\n", Y_SPECIE_CO);
	#endif
	#ifdef Y_SPECIE_SO2
		Message("	Y_SPECIE_SO2 %d\n", Y_SPECIE_SO2);
	#endif
	#ifdef Y_SPECIE_H2
		Message("	Y_SPECIE_H2  %d\n", Y_SPECIE_H2);
	#endif
	#ifdef Y_SPECIE_N2
		Message("	Y_SPECIE_N2  %d\n", Y_SPECIE_N2);
	#endif
	#ifdef Y_SPECIE_CH4
		Message("	Y_SPECIE_CH4 %d\n", Y_SPECIE_CH4);
	#endif
	#ifdef Y_SPECIE_TAR
		Message("	Y_SPECIE_TAR %d\n", Y_SPECIE_TAR);
	#endif


	Message("\nCoal properties from coalprop.h:\n");
	Message("\nMass fractions proximate analysis of coal\n \
		COAL_H2O %.4f\n \
		COAL_VOL %.4f\n \
		COAL_CHAR %.4f\n \
		COAL_ASH %.4f\n", COAL_H2O,COAL_VOL,COAL_CHAR,COAL_ASH);
		
	Message("\nMass fractions ultimate analysis in coal\n \
		COAL_C %.4f\n \
		COAL_H %.4f\n \
		COAL_O %.4f\n \
		COAL_N %.4f\n \
		COAL_S %.4f\n", COAL_C,COAL_H,COAL_O,COAL_N,COAL_S);
		 
	Message("\nDefinition of zero\n \
		ZERO %e\n", ZERO);
}
