/**************************************************************************************************
 * FLUENT MACRO particle_Update_Source
/**************************************************************************************************
 * Function:    Puts the previously calculated source terms from particle
 *              		combustion to the gas phase. 
 *
 * Usage:       	Plug into Define->Models->Discrete phase...->UDF->Source
 *
 * Input:
 * cell_t  *c         				Pointer to structure c (Cell)
 * Thread  *t         				Pointer to structure t (Tread)
 * dpms_t  *s         				Pointer to structure s (Source terms)
 * double   strength  		Number of particles in parcel
 * Tracked_Particle  *p  	Pointer to structure p (Particle)
 *
 * double        	P_T(p)    											particle temperature
 * double        	P_VEL(p)  											particle velocity in x-, y- and z-direction
 * double 			const  P_T_REF   								Reference temperature for enthalpy
 * double  			DPM_SPECIFIC_HEAT(p,P_T(p))   heat capacity of particle
 *
 * int const  NUMBER_OF_SPECIES  number of species in mixture
 *
 * Output:
 * double  C_DPMS_YI(P_CELL(p), P_CELL_THREAD(p), i)    					species source terms
 * double  C_DPMS_ENERGY(P_CELL(p), P_CELL_THREAD(p))				energy source term
 * double  C_SOURCE_MASS_DPM(P_CELL(p), P_CELL_THREAD(p))  	mass source term
 * double  C_DPMS_MOM_S(P_CELL(p), P_CELL_THREAD(p))[i]    		momentum source terms
 *************************************************************************************************/
#include "Preamble.h"
#include "PRI.h"

DEFINE_DPM_SOURCE(particle_Update_Source, c, t, S, strength, p)              
{
	int i ;
	double Tcell = C_T(c,t) ;
	double m_sum = 0 ; 		/* sum of released mass */
	double t_p = P_T(p) ; 		/* particle temperature */
	double cp_p = DPM_SPECIFIC_HEAT(p, P_T(p)) ; /* particle heat capacity */
	double const t_p_ref = P_T_REF ; /* Reference temperature for enthalpy */
	double u_p = P_VEL(p)[0] ; /* velocity in x-direction */
	double v_p = P_VEL(p)[1] ; /* velocity in y-direction */
	double w_p = P_VEL(p)[2] ; /* velocity in z-direction */
	double m_rem_p = P_USER_REAL(p , 16) ; /* mass change due to particle removal */
	double h_sum = P_USER_REAL(p , 17) ; 		/* sum of released enthalpy */
	double m_sum_spec[NUMBER_OF_SPECIES] = { P_USER_REAL(p,18), \
	                     P_USER_REAL(p,19), \
						 P_USER_REAL(p,20), \
						 P_USER_REAL(p,21), \
						 P_USER_REAL(p,22), \
						 P_USER_REAL(p,23), \
						 P_USER_REAL(p,24), \
						 P_USER_REAL(p,25), \
						 P_USER_REAL(p,26), \						 
						 P_USER_REAL(p,27), \
						 P_USER_REAL(p,28) };
	double burnout = P_USER_REAL(p,29); /* particle burnout */
	double Qsca = 1.15 ; /* Scattering efficiency */
	double Qabs = 2 ;		 /* Absorption efficiency */
	/* End input */
	/* Begin output */
	/* Mass sources, loop over all species */
	for (i = 0; i < NUMBER_OF_SPECIES; i++) {
		m_sum += m_sum_spec[i];
		S->species[i] += -m_sum_spec[i] * strength;
	}

	/* In order to keep the mass, energy and impuls balance nice and tidy 
	   FLUENT puts the difference between the entry and exit values of 
	   those quantities of the particles as source terms to the gas phase. 
	   This forces us to do some cleanup in order to avoid a complete
	   mess in our calculations. We have to
	   - remove heat, that goes to the particle but does not come from
	     the gas phase. Sources for such heat are chemical reactions, 
	     radiation and the removal of particles when just ash is left.
	   - put heat, that goes to the particle unnoticed by FLUENT, such as
	     the heat used for evaporating water.
	   - remove mass, that leaves the particle but does not belong into the
	     gas phase, like the mass, which shows up when a particle is 
	     removed when just ash is left.
	   - remove impulse, that leaves the particle but does not belong into
	     the gas phase, like the impulse, which shows up when a particle 
	     is removed when just ash is left. */

	/* Particle Burnout */
	C_DPMS_BURNOUT(P_CELL(p),P_CELL_THREAD(p)) += -burnout * strength;
	/* Energy */
	C_DPMS_ENERGY(P_CELL(p),P_CELL_THREAD(p)) += h_sum * strength;


	/* Remove what FLUENT puts due to removal of ash */
	C_SOURCE_MASS_DPM(P_CELL(p),P_CELL_THREAD(p)) += m_rem_p * strength;

	C_DPMS_ENERGY(P_CELL(p),P_CELL_THREAD(p)) += m_rem_p * strength
			* cp_p * ( t_p - t_p_ref ); 

	C_DPMS_MOM_S(P_CELL(p),P_CELL_THREAD(p))[0] += m_rem_p * u_p * strength; /* x-direction */
	C_DPMS_MOM_S(P_CELL(p),P_CELL_THREAD(p))[1] += m_rem_p * v_p * strength; /* y-direction */
	C_DPMS_MOM_S(P_CELL(p),P_CELL_THREAD(p))[2] += m_rem_p * w_p * strength; /* z-direction */
	
	C_DPMS_SCAT(P_CELL(p),P_CELL_THREAD(p)) = PRI_ScatCoeff(C_DPMS_CONCENTRATION(c,t),Dp, Rhop, Qsca) * C_VOLUME(P_CELL(p),P_CELL_THREAD(p)) ;
	C_DPMS_ABS(P_CELL(p),P_CELL_THREAD(p)) = PRI_AbsCoeff(C_DPMS_CONCENTRATION(c,t), Dp, Rhop, Qabs) * C_VOLUME(P_CELL(p),P_CELL_THREAD(p)) ;
	C_DPMS_EMISS(P_CELL(p),P_CELL_THREAD(p)) = PRI_AbsCoeff(C_DPMS_CONCENTRATION(c,t), Dp, Rhop, Qabs) * C_VOLUME(P_CELL(p),P_CELL_THREAD(p)) * pow(Tcell,4) ;
	
/* Reset source terms for next cell/particle */
	P_USER_REAL(p,16) = 0.;
	P_USER_REAL(p,17) = 0.;
	P_USER_REAL(p,18) = 0.;
	P_USER_REAL(p,19) = 0.;
	P_USER_REAL(p,20) = 0.;
	P_USER_REAL(p,21) = 0.;
	P_USER_REAL(p,22) = 0.;
	P_USER_REAL(p,23) = 0.;
	P_USER_REAL(p,24) = 0.;
	P_USER_REAL(p,25) = 0.;
	P_USER_REAL(p,26) = 0.;
	P_USER_REAL(p,27) = 0.;
	P_USER_REAL(p,28) = 0.;
	P_USER_REAL(p,29) = 0.;	
	
	return;
}