/*	Test UDF for Scattering Phase Functions	*/

#include "udf.h"
DEFINE_SCAT_PHASE_FUNC(ScatPhiF2,c,fsf) {
	real phi=0;
	real coeffs[9]={1,2.00917,1.56339,0.67407,0.22215,0.04725,0.00671,0.00068,0.00005};
	real P[9];
	int i;
	*fsf = 0;
	P[0] = 1;
	P[1] = c;
	phi = P[0]*coeffs[0] + P[1]*coeffs[1];
	for(i=1;i<7;i++){
		P[i+1] = 1/(i+1.0)*((2*i+1)*c*P[i] - i*P[i-1]);
		phi += coeffs[i+1]*P[i+1];
	}
	return (phi);
}