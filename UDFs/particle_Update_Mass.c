/*************************************************************************************
 * Update_Mass
/*************************************************************************************
 * Function     Keeps track of the mass and composition of the particle
 *
 * Input:
 * Tracked_Particle  *p     							Pointer to structure p
 * double  					*dm_spec_drying  	species mass exchange from evaporation
 * double  					*dm_spec_dev     		species mass exchange from devolatilisation 
 * double  					*dm_spec_char    		species mass exchange from char burnout
 * double  					m_rem_p          			mass of removed ash
 * double  					*m_elem_p[]      		composition of particle
 * double  					*m_elem_dev[]    		composition of volatiles
 * double  					P_DT(p)          			duration of particle time step 
 * int const  				NUMBER_OF_SPECIES  
 *
 * Output:
 * double  					*m_elem_p[]    			composition of particle
 * double  					*m_elem_dev[]  		composition of volatiles
 * double  					P_MASS(p)      			mass of particle
 *************************************************************************************/
#include <stdio.h>
#include "Preamble.h"

void Update_Mass_P(Tracked_Particle *p, double *dm_spec_drying, 
                   double *dm_spec_dev, double *dm_spec_char, 
                   double m_rem_p, Particle_Composition *pc) {

	double dt = P_DT(p); /* duration of particle time step */
	double n_el_spec[NUMBER_OF_SPECIES][NUMBER_OF_ELEMENTS] = N_EL_SPEC;
	double mm_el[NUMBER_OF_ELEMENTS] = MM_EL;
	/* End input */
	int i_el, i_spec;
	double mm_spec;
	double dm_sum_water = 0;
	double dm_sum_vol = 0;
	double dm_sum_char = 0;
	double dm_spec_sum[NUMBER_OF_SPECIES] = { 0 };
	
	static double el_frac_spec[NUMBER_OF_SPECIES][NUMBER_OF_ELEMENTS] = {{0}};
	static int mfrac_table_init = 0;
	
	/* Build and store table of mass fractions of elements in species once */
	if (!mfrac_table_init) {
		for (i_spec = 0; i_spec < NUMBER_OF_SPECIES; i_spec++) {
			/* molar mass of species */
			mm_spec = 0;
			for (i_el = 0; i_el < NUMBER_OF_ELEMENTS; i_el++) {
				mm_spec += n_el_spec[i_spec][i_el]*mm_el[i_el];
			}
			if (!mm_spec) {
				Message("pUM: Error! mm_spec zero.\n");
			}
			/* mass fraction of element in species */
			for (i_el = 0; i_el < NUMBER_OF_ELEMENTS; i_el++) {
				el_frac_spec[i_spec][i_el] = 
				    n_el_spec[i_spec][i_el]*mm_el[i_el]/mm_spec;
			}
		}
		mfrac_table_init = 1;
	}

	/* Begin output */
	/* update amount of water, vol and char */
	for (i_spec=0; i_spec < NUMBER_OF_SPECIES; i_spec++) {
		dm_sum_water += dm_spec_drying[i_spec];
		dm_sum_vol += dm_spec_dev[i_spec];
		dm_sum_char += dm_spec_char[i_spec];
	}
	pc->water_remain += dm_sum_water * dt;
	pc->vol_remain += dm_sum_vol * dt;
	pc->char_remain += dm_sum_char* dt;
	
	/* update mass of particle */
	P_MASS(p) += (dm_sum_vol + dm_sum_char + dm_sum_water) * dt + m_rem_p;
	
	/* update elements of vol and char remaining */
	for (i_el = 0; i_el < NUMBER_OF_ELEMENTS; i_el++) {
		for (i_spec=0; i_spec < NUMBER_OF_SPECIES; i_spec++) {
			
			pc->m_elem_vol[i_el] += el_frac_spec[i_spec][i_el]
			                    * dm_spec_dev[i_spec] * dt;
			pc->m_elem_char[i_el] += el_frac_spec[i_spec][i_el]
			                     * dm_spec_char[i_spec] * dt;
		}
	}
	return;	
}