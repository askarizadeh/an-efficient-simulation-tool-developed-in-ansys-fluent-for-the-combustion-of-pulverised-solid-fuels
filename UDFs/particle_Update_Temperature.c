/******************************************************************************
 * Update_Temperature
 ******************************************************************************
 * Function: 
 * Calculation of temperature change of particle due to heat exchange with 
 * surroundings and enthalpie going to or coming from particle due to 
 * evaporation, devolatilisation and char burnout. Overall positive values for 
 * the enthalphy increase the temperature of the particle.
 *
 * Input:
 * Tracked_particle  	*p  					pointer to structure p containing particle information
 * double  						dh_dev        	devolatilisation enthalphy going to the particle
 * double  						dh_char       	char burnout enthalphy going to the particle
 * double  						dh_transfer   	heat going to particle due to convection and radiation
 * int     							evap          		boolean indicating evaporation
 *
 * double  P_MASS(p)  				                        particle mass
 * double  P_T(p)                          					    particle temperature
 * double  P_DIAM(p)                               			particle diameter
 * double  P_Re(p)                               					particle Reynolds number
 * double  P_DT(p)                             					duration of time step
 * double  DPM_SPECIFIC_HEAT(p,P_T(p))     heat capacity of particle
 * double  EPSILON_P                           				emissivity of particle
 * int		  DPM_RADIATION_P                     	boolean indicating radiation
 * double  C_T(P_CELL(p), P_CELL_THREAD(p))    		temperature of gas
 * double  C_CP(P_CELL(p),P_CELL_THREAD(p))    		heat capacity of gas
 * double  C_K_L(P_CELL(p),P_CELL_THREAD(p))   		thermal conductivity of gas
 * double  C_MU_T(P_CELL(p),P_CELL_THREAD(p)) 	dynamic viscosity
 * double  C_G                                 										incident radiation
 * double  const  SIGMASBCONST                  					Stefan Boltzmann constant
 *
 * Output:
 * double  P_T(p)       		New temperature of the particle
 * double *dh_transfer  	Heat going to particle due to convection and radiation
 *****************************************************************************/
#include <math.h>
#include <stdio.h>
#include "Preamble.h"

void Update_Temperature_P(Tracked_Particle *p, double dh_dev, double dh_char, 
                          double *dh_transfer, int evap) 
{
	double const sigma_sb = SIGMA_SBCONST;          					/* Stefan Boltzmann constant */
	double const pi = PI; /* ratio between a circles circumference to its diameter in Euclidean geometry */
	double t_g = C_T(P_CELL(p), P_CELL_THREAD(p));  			/* gas temperature */
	double t_p = P_T(p);                            										/* particle temperature */
	double dt = P_DT(p);                            										/* duration of time step */
	double m_p = P_MASS(p);                         									/* particle mass */
	double cp_p = DPM_SPECIFIC_HEAT(p,P_T(p));      			/* heat capacity of particle */
	double d_p = P_DIAM(p);                         									/* particle diameter */
	double epsilon_p = EPSILON_P ;												/* particle emissivity */
	double k_g = C_K_L(P_CELL(p),P_CELL_THREAD(p)); 		/* thermal conductivity of gas*/
	double mu = C_MU_T(P_CELL(p),P_CELL_THREAD(p)); 	/* dynamic viscosity */
	double cp_g = C_CP(P_CELL(p),P_CELL_THREAD(p)); 		/* heat capacity of gas */
	double Re = P_Re(p);                              									/* particle Re-number */
#if UDF_ENVIRONMENT == FLUENT	
	cphase_state_t *c = &(p->cphase);
#endif
	double inc_rad = C_G; /* incident radiation in cell */	
	/* End input */
	
	double surf_area = pi * d_p*d_p;                				/* particle surface area */
	double Pr = mu * cp_g / k_g;                    					/* Prandtl Number */ 
	double Nu = 2. + 0.6 * sqrt(Re)*pow(Pr,1./3.);  	/* Nusselt Number: Ranz et Marshall*/
	double h = Nu * k_g / d_p;                      					/* heat transfer coefficient */


#if UDF_ENVIRONMENT == PLUGFLOW
	Nu = 2.;
	double t_film=(t_p+t_g)/2.0;
	k_g = (7e-09*pow(t_film,3.0) - 2e-05*pow(t_film,2.0) 
	                           + 0.0744*t_film + 5.436)*1e-3;
	h = Nu*k_g/d_p;  /* heat transfer coefficient */
#endif


	double alpha;
	double beta;
	double expbeta;
	double dh_sum = dh_dev + dh_char;
	double p_t_prev = t_p; /* store previous temperature */
	
	if (evap) {
		/* P_T = const */
		/* dh_transfer is considered in dh_drying */
		/* gives small error for last time step where evap takes place
		 * but does not last for whole time step */
	} else {
		if (DPM_RADIATION_P) {
			alpha = (h*t_g + dh_sum/surf_area + 0.25*epsilon_p*inc_rad) /
				(h + epsilon_p*sigma_sb*pow(t_p,3));
			beta = dt*surf_area*(h+epsilon_p*sigma_sb*pow(t_p,3)) /
				(m_p*cp_p);
		} else {
			alpha = (h*t_g + dh_sum/surf_area) / h;
			beta = (h * surf_area * dt) / (m_p*cp_p);
		}
				
		if (beta > 50) {
			expbeta = 0.;
		} else if (beta < 0.01) {
			expbeta = (1-beta+0.5*beta*beta);
		} else {
			expbeta = exp(-beta);
		}
			
		t_p = alpha + (p_t_prev-alpha) * expbeta;

		/* Begin output */
		P_T(p) = t_p;
		*dh_transfer = m_p * cp_p * (t_p - p_t_prev) / dt - dh_sum;
	}

}